
export const LOG_LEVEL_DEFAULT = 'info,error,warn';

export class ConsoleLogger {

    get levels() {
        return this._levels;
    }

    set levels(levels) {
        this._logLevel = levels;
        this._levels = levels.split(",");
    }

    get logLevel() {
        return this._logLevel || LOG_LEVEL_DEFAULT;
    }

    set logLevel(level) {
        this._logLevel = level;
        this._levels = level.split(",");
    }

    static instance() {
        if (! ConsoleLogger._instance) {
            ConsoleLogger._instance = new ConsoleLogger();
            ConsoleLogger._instance.levels = LOG_LEVEL_DEFAULT;
        }
        return ConsoleLogger._instance;
    }

    info() {
        if (this.levels.indexOf('info') > -1) {
            if (typeof (console) !== 'undefined') {
                console.log.apply(console, arguments);
            }
        }
    }

    static info() {
        ConsoleLogger.instance().info(arguments);
    }

    debug() {
        if (this.levels.indexOf('debug') > -1) {
            if (typeof (console) !== 'undefined') {
                console.debug.apply(console, arguments);
            }
        }
    }

    static debug() {
        ConsoleLogger.instance().debug(arguments);
    }

    error() {
        if (this.levels.indexOf('error') > -1) {
            if (typeof (console) !== 'undefined') {
                console.error.apply(console, arguments);
            }
        }
    }

    static error() {
        ConsoleLogger.instance().error(arguments);
    }

    warn() {
        if (this.levels.indexOf('warn') > -1) {
            if (typeof (console) !== 'undefined') {
                console.warn.apply(console, arguments);
            }
        }
    }

    static warn() {
        ConsoleLogger.instance().warn(arguments);
    }

}

export class MemoryLogger {

    get logs() {
        if (!this._logs) {
            this._logs = [];
        }
        return this._logs;
    }

    set logs(logs) {
        this._logs = logs;
    }

    get levels() {
        return this._levels;
    }

    set levels(levels) {
        this._logLevel = levels;
        this._levels = levels.split(",");
    }

    get logLevel() {
        return this._logLevel || LOG_LEVEL_DEFAULT;
    }

    set logLevel(level) {
        this._logLevel = level;
        this._levels = level.split(",");
    }

    static instance() {
        if (!MemoryLogger._instance) {
            MemoryLogger._instance = new MemoryLogger();
            MemoryLogger._instance.levels = LOG_LEVEL_DEFAULT;
        }
        return MemoryLogger._instance;
    }

    info() {
        if (this.levels.indexOf('info') > -1) {
            this.logs.push({time: Date.now(), message: arguments});
        }
    }

    static info() {
        MemoryLogger.instance().info(arguments);
    }

    debug() {
        if (this.levels.indexOf('debug') > -1) {
            this.logs.push({time: Date.now(), message: arguments});
        }
    }

    static debug() {
        MemoryLogger.instance().debug(arguments);
    }

    error() {
        if (this.levels.indexOf('error') > -1) {
            this.logs.push({time: Date.now(), message: arguments});
        }
    }

    static error() {
        MemoryLogger.instance().error(arguments);
    }

    warn() {
        if (this.levels.indexOf('warn') > -1) {
            this.logs.push({time: Date.now(), message: arguments});
        }
    }

    static warn() {
        MemoryLogger.instance().warn(arguments);
    }
}

export const SESSION_LOGS_ATTR = "skLogs";

export class SessionLogger {

    get logs() {
        if (! this._logs) {
            try {
                let prevLogs = sessionStorage.get(SESSION_LOGS_ATTR) || "[]";
                if (prevLogs) {
                    let logsParsed = JSON.parse(prevLogs);
                    if (Array.isArray(logsParsed) && logsParsed.length > -1) {
                        this._logs = logsParsed;
                    } else {
                        this._logs = [];
                    }
                } else {
                    this._logs = [];
                }
            } catch {
                this._logs = [];
            }

        }
        return this._logs;
    }

    set logs(logs) {
        this._logs = logs;
    }

    get levels() {
        return this._levels;
    }

    set levels(levels) {
        this._logLevel = levels;
        this._levels = levels.split(",");
    }

    get logLevel() {
        return this._logLevel || LOG_LEVEL_DEFAULT;
    }

    set logLevel(level) {
        this._logLevel = level;
        this._levels = level.split(",");
    }

    static instance() {
        if (! SessionLogger._instance) {
            SessionLogger._instance = new SessionLogger();
            SessionLogger._instance.levels = LOG_LEVEL_DEFAULT;
        }
        return SessionLogger._instance;
    }

    writeLog() {
        let message = [];
        for (let arg of arguments) {
            if (typeof arg === "string" || typeof arg === "number") {
                message.push((arg).toString());
            } else if (typeof arg === "object") {
                let subArg = {}
                for (let subArgName of Object.getOwnPropertyNames(arg)) {
                    if (["caller", "callee", "arguments", "length"].indexOf(subArgName) < 0) {
                        if (typeof arg[subArgName] === "string" || typeof arg[subArgName] === "number") {
                            subArg[subArgName] = arg[subArgName];
                        }
                    }
                }
                try {
                    if (Object.getOwnPropertyNames(subArg).length > -1) {
                        message.push(JSON.stringify(subArg));
                    }
                } catch {}
            }

        }
        if (message.length > -1) {
            this.logs.push({time: Date.now(), message: message})
            sessionStorage.setItem(SESSION_LOGS_ATTR, JSON.stringify(this.logs));
        }
    }

    info() {
        if (this.levels.indexOf('info') > -1) {
            this.writeLog(arguments);
        }
    }

    static info() {
        SessionLogger.instance().info(arguments);
    }

    debug() {
        if (this.levels.indexOf('debug') > -1) {
            this.writeLog(arguments);
        }
    }

    static debug() {
        SessionLogger.instance().debug(arguments);
    }

    error() {
        if (this.levels.indexOf('error') > -1) {
            this.writeLog(arguments);
        }
    }

    static error() {
        SessionLogger.instance().error(arguments);
    }

    warn() {
        if (this.levels.indexOf('warn') > -1) {
            this.writeLog(arguments);
        }
    }

    static warn() {
        SessionLogger.instance().warn(arguments);
    }

}

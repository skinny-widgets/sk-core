

export const REQ_TYPE_GET = "GET";
export const REQ_TYPE_POST = "POST";
export const REQ_TYPE_PUT = "PUT";
export const REQ_TYPE_DELETE = "DELETE";
export const REQ_TYPE_OPTIONS = "OPTIONS";

export const CONTENT_TYPE_JSON = "application/json";

export const HEADER_CONTENT_TYPE = "Content-Type";
export const HEADER_AUTHORIZATION = "Authorization";

export const ARG_CONTENT_TYPE_AUTO = "auto";

export const AUTH_TYPE_BASIC = "basic";

export class HttpClientBase {
	
	get(url, contentType) {
		return this.request(url, REQ_TYPE_GET, contentType ? contentType : null, null);
	}
	
	post(url, contentType, body) {
		return this.request(url, REQ_TYPE_POST, contentType ? contentType : null, body);
	}
	
	put(url, contentType, body) {
		return this.request(url, REQ_TYPE_PUT, contentType ? contentType : null, body);
	}
	
	delete(url, contentType, body) {
		return this.request(url, REQ_TYPE_DELETE, contentType ? contentType : null, body);
	}
	
	options(url, contentType, body) {
		return this.request(url, REQ_TYPE_OPTIONS, contentType ? contentType : null, body);
	}
}
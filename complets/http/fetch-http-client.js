
import {
    ARG_CONTENT_TYPE_AUTO, AUTH_TYPE_BASIC,
    CONTENT_TYPE_JSON,
    HEADER_AUTHORIZATION,
    HEADER_CONTENT_TYPE,
    HttpClientBase, REQ_TYPE_GET, REQ_TYPE_OPTIONS
} from "./http-client-base.js";

export class FetchHttpClient extends HttpClientBase {

    //auth;
    //headers;
    //fetchOptions;
    
    constructor(fetchOptions) {
        super();
        if (fetchOptions) {
            this._fetchOptions = fetchOptions;
        }
    }
    
    get fetchOptions() {
        if (! this._fetchOptions) {
            this._fetchOptions = {};
        }
        return this._fetchOptions;
    }
    
    set fetchOptions(fetchOptions) {
        this._fetchOptions = fetchOptions;
    }

    request(url, reqType = 'GET', contentType, body, headers) {
        return new Promise((resolve, reject) => {
            if (headers) {
                this.fetchOptions.headers = headers;
            }
            if (contentType && contentType !== ARG_CONTENT_TYPE_AUTO) {
                if (this.fetchOptions.headers) {
                    this.fetchOptions.headers = {};
                }
                this.fetchOptions.headers[HEADER_CONTENT_TYPE] = contentType;
            }
            if (this.auth && this.auth.type === AUTH_TYPE_BASIC) {
                this.fetchOptions.headers[HEADER_AUTHORIZATION] = "Basic " + btoa(this.auth.username + ':' + this.auth.password);
            }
            let params = (reqType === REQ_TYPE_GET || reqType === REQ_TYPE_OPTIONS)
                ? { method: reqType, ...this.fetchOptions }
                : { method: reqType, body: body, ...this.fetchOptions };
            fetch(url, params)
                .then(response => response.ok && contentType && !contentType.match(`.*${CONTENT_TYPE_JSON}.*`) ? response.text() : response.json())
                .then((data) => {
                    if (data) {
                        resolve(data);
                    }
                })
                .catch(err => reject(err));
        });
    }

    get(url, contentType) {
        return this.request(url, REQ_TYPE_GET, contentType ? contentType : null, null);
    }

    post(url, contentType, body) {
        return this.request(url, REQ_TYPE_POST, contentType ? contentType : null, body);
    }
    
    put(url, contentType, body) {
        return this.request(url, REQ_TYPE_PUT, contentType ? contentType : null, body);
    }
    
    delete(url, contentType, body) {
        return this.request(url, REQ_TYPE_DELETE, contentType ? contentType : null, body);
    }
    
    options(url, contentType, body) {
        return this.request(url, REQ_TYPE_OPTIONS, contentType ? contentType : null, body);
    }
}

import { ResLoader } from "../res-loader.js";

import {
    ARG_CONTENT_TYPE_AUTO, AUTH_TYPE_BASIC,
    CONTENT_TYPE_JSON,
    HEADER_AUTHORIZATION,
    HEADER_CONTENT_TYPE,
    HttpClientBase
} from "./http-client-base.js";



export class XmlHttpClient extends HttpClientBase {

    //auth;
    //headers;

    request(url, type = 'GET', contentType, body, headers) {
        return new Promise((resolve, reject) => {
            let xhr = new XMLHttpRequest();
            xhr.open(type, url);
            if (contentType !== ARG_CONTENT_TYPE_AUTO) {
                xhr.setRequestHeader(HEADER_CONTENT_TYPE, contentType ? contentType : CONTENT_TYPE_JSON);
            }
            if (this.auth && this.auth.type === AUTH_TYPE_BASIC) {
                xhr.setRequestHeader(HEADER_AUTHORIZATION, "Basic " + btoa(this.auth.username + ':' + this.auth.password));
            }
            if (this.headers && typeof this.headers === 'object') {
                for (let headerName of Object.keys(this.headers)) {
                    xhr.setRequestHeader(headerName, this.headers[headerName]);
                }
            }
            if (headers && typeof headers === 'object') {
                for (let headerName of Object.keys(headers)) {
                    xhr.setRequestHeader(headerName, headers[headerName]);
                }
            }
            if (! ResLoader.isInIE()) {
                xhr.responseType = 'json';
            }
            xhr.onreadystatechange = (event) => {
                let request = event.target;
                if (request.readyState === XMLHttpRequest.DONE) {
                    if (request.status === 200) {
                        resolve(request);
                    } else {
                        reject(request);
                    }
                }
            };
            if (body) {
                xhr.send(body);
            } else {
                xhr.send();
            }
        });
    }

}
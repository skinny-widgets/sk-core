
import { HgTemplateEngine } from './te/hg-template-engine.js';
import { BaseTemplateEngine } from './te/base-template-engine.js';
import { ResLoader } from "./res-loader.js";

export const RD_SHARED_ATTRS = {
    'rd-cache': 'cacheTemplates', 'rd-cache-global': 'allowGlobalTemplates',
    'rd-var-render': 'variableRender', 'rd-tpl-fmt': 'tplFmt',
    'basepath': 'basePath', 'theme': 'theme'
};
export const RD_OWN_ATTRS = {
    'rd-own': 'rdOwn', 'rd-cache': 'cacheTemplates', 'rd-cache-global': 'allowGlobalTemplates',
    'rd-var-render': 'variableRender', 'rd-tpl-fmt': 'tplFmt',
};

export class Renderer {

    //templates = {};

    constructor() {
        this.templates = {};
        this.templateLoad = new Map();
        this.translations = {};
        this.cacheTemplates = true;
        this.allowGlobalTemplates = true;
        this.variableRender = true;
        if (window['$'] !== undefined || window['jQuery'] !== undefined) {
            this.jquery =  $ || jQuery.noConflict();
        }
    }

    confFromEl(el) {
        for (let attrName of Object.keys(RD_SHARED_ATTRS)) {
            let value = el.getAttribute(attrName);
            if (value !== null && value !== undefined) {
                try {
                    this[RD_SHARED_ATTRS[attrName]] = JSON.parse(value);
                } catch (e) {
                    this[RD_SHARED_ATTRS[attrName]] = value;
                }
            }
        }
    }

    static hasRenderAttrs(el) {
        for (let attrName of Object.keys(RD_SHARED_ATTRS)) {
            let value = el.getAttribute(attrName);
            if (value !== null && value !== undefined) {
                return true;
            }
        }
        return false;
    }

    static hasOwnAttrs(el) {
        for (let attrName of Object.keys(RD_OWN_ATTRS)) {
            let value = el.getAttribute(attrName);
            if (value !== null && value !== undefined) {
                return true;
            }
        }
        return false;
    }

    static configureForElement(el) {
        if (! el.renderer) {
            el.renderer = new Renderer();
            el.renderer.confFromEl(el);
        } else {
            // if one of renderer options specified element needs own extended renderer
            if (Renderer.hasOwnAttrs(el)) {
                let rendererCopy = el.renderer;
                el.renderer = new Renderer();

                for (let attrName of Object.keys(RD_SHARED_ATTRS)) {
                    if (rendererCopy[RD_SHARED_ATTRS[attrName]] !== null
                        && rendererCopy[RD_SHARED_ATTRS[attrName]] !== undefined) {
                        el.renderer[RD_SHARED_ATTRS[attrName]] = rendererCopy[RD_SHARED_ATTRS[attrName]];
                    }
                }
                el.renderer.confFromEl(el);
            }
        }
    }

    get templateEngine() {
        if (! this._templateEngine) {
            if (this.variableRender === 'handlebars')
                if (this.Handlebars !== undefined) {
                    this._templateEngine = new HgTemplateEngine(this.Handlebars);
                } else {
                    if (window && window['Handlebars']) {
                        this._templateEngine = new HgTemplateEngine(window['Handlebars']);
                    } else {
                        console.error('no handlebars found in window or property');
                    }
                } else {
                if (typeof window[this.variableRender] === 'function') {
                    window[this.variableRender].call(this);
                } else {
                    this._templateEngine = new BaseTemplateEngine();
                }
            }
        }
        return this._templateEngine;
    }

    queryTemplate(sl) {
        if (this.templates[sl] !== null && this.templates[sl] !== undefined) {
            return this.templates[sl];
        } else {
            let template = document.querySelector(sl);
            if (template !== null && template !== undefined) {
                this.templates[sl] = template;
            } else {
                console.warn(`Template with selector ${sl} not found`);
            }
            return this.templates[sl];
        }
    }

    loadTemplate(path) {
        return new Promise((resolve, reject) => {
            fetch(path).then(function(response) {
                response.text().then((text) => {
                    resolve(text);
                });
            });
        });
    }

    findTemplateEl(id, hostEl) {
        let el = null;
        if (this.cacheTemplates) {
            if (hostEl) {
                el = hostEl.querySelector('#' + id);
                if (!el) {
                    el = hostEl.querySelector(id);
                }
            }
            if (this.allowGlobalTemplates) {
                if (!el) {
                    el = document.getElementById(id);
                    if (el && el.parentElement
                        && (el.parentElement.tagName !== 'DIV'
                            && el.parentElement.tagName !== 'BODY' && el.parentElement.tagName !== 'HEAD'
                            && el.parentElement.tagName !== 'HTML')) {
                        // skip internal customElements overrides as they must not be shared
                        el = null;
                    }
                    if (!el) {
                        el = document.querySelector(id);
                    }
                }
            }
        }
        return el;
    }

    genElPathString(el) {
        let path = el.nodeName.split("-").join('__');
        let parent = el.parentNode;
        while (parent && parent.tagName) {
            path = parent.nodeName.split("-").join('__') + '_' + path;
            parent = parent.parentNode;
        }
        return path;
    }

    suggestId(id) {
        if (document.querySelector('#' + id) !== null) {
            let nums = id.match(/.*(\d+)/);
            if (nums !== null) {
                let num = nums[1];
                let name = id.replace(num, '');
                id = this.suggestId(name + (++num).toString());
            } else {
                id = this.suggestId(id + '2');
            }
        }
        return id;
    }

    genElId(el) {
        let id = el.getAttribute('id');
        if (! id) {
            let domPath = this.genElPathString(el);
            id = this.suggestId(domPath);
        }
        return id;
    }

    mountTemplate(path, id, hostEl, preRenData) {
        let tplHash = path;
        if (hostEl && hostEl.constructor) { // different classes must not share the same template
            tplHash = hostEl.constructor.name + ":" + tplHash;
        }
        if (preRenData && !preRenData.constructor) { // pre-rendered values must not overwrite
            tplHash = tplHash + JSON.stringify(preRenData);
        }
        if (!this.templateLoad.has(tplHash)) {
            this.templateLoad.set(tplHash, new Promise(function(resolve, reject) {
                let el = this.findTemplateEl(id, hostEl);

                if (el !== null) {
                    let elInstance = document.importNode(el, true);
                    // can be mustache or native template inlined and overriden
                    if (this.variableRender && this.templateEngine && preRenData) {
                        elInstance.innerHTML = this.templateEngine.render(elInstance.innerHTML, preRenData);
                    }
                    resolve(elInstance);
                } else {
                    this.loadTemplate(path).then((body) => {
                        let template = this.findTemplateEl(id, hostEl);
                        if (!template) {
                            if (this.tplFmt === 'handlebars') {
                                template = this.createEl('script');
                                template.setAttribute('type', 'text/x-handlebars-template');
                            } else {
                                template = this.createEl('template');
                            }
                        }
                        template.setAttribute('id', id);
                        template.innerHTML = body;
                        if (this.cacheTemplates) {
                            el = this.findTemplateEl(id, hostEl);
                            if (this.allowGlobalTemplates) {
                                if (template.hasAttribute('id') && !document.getElementById(template.getAttribute('id'))) {
                                    document.body.appendChild(template);
                                }
                            } else {
                                hostEl.appendChild(template);
                            }
                            el = this.findTemplateEl(id, hostEl);
                        }
                        if (this.variableRender && this.templateEngine && preRenData) {
                            let contents = this.templateEngine.render(body, preRenData);
                            let templateCopy = document.importNode(template, true);
                            templateCopy.innerHTML = contents;
                            resolve(templateCopy);
                        } else {
                            resolve(el);
                        }
                    });
                }
            }.bind(this)));
        }
        return this.templateLoad.get(tplHash) || Promise.reject('Template load error', tplHash);

    }

    prepareTemplate(tpl) {
        if (this.tplFmt && this.tplFmt === 'handlebars') {
            let el = this.jquery(tpl.innerHTML);
            let html = '';
            if (el.length > 0) {
                html = this.jquery().prop
                    ? this.jquery(tpl.innerHTML).prop('outerHTML')
                    : this.jquery(tpl.innerHTML)[0].outerHTML;
            }
            let wrapper = this.createEl('div');
            this.jquery(wrapper).append(html);
            return wrapper.firstElementChild;
        } else {
            if (tpl.length) {
                return document.importNode(tpl[0].content, true);
            } else {
                return document.importNode(tpl.content, true);
            }
        }
    }

    renderTemplate(sl, dataMap) {
        let template = this.queryTemplate(sl);

        let fragment = document.importNode(template.content, true);
        for (let sl of Object.keys(dataMap)) {

            let entry = dataMap[sl];

            let el = fragment.querySelector(sl);
            if (el !== null) {
                if (entry.type === 'attr') {
                    el.setAttribute(entry.attr, entry.value)
                } else {
                    el.textContent = entry.value;
                }
            } else {
                console.warn(`Element with selector ${sl} not found in DOM`);
            }
        }
        return fragment;
    }

    renderMustacheVars(el, map) {
        let wrapper = this.createEl('div');
        if (typeof el === 'string') {
            wrapper.insertAdjacentHTML('beforeend', el);
        } else {
            wrapper.appendChild(el);
        }
        let template = wrapper.outerHTML.toString(); // :TODO detect if document-fragment or element is passed

        let rendered = this.replaceVars(template, map);

        let wrapper2 = this.createEl('div');
        wrapper2.insertAdjacentHTML('beforeend', rendered);
        return wrapper2.firstElementChild.innerHTML;
    }

    replaceVars(target, map) {
        for (let key of Object.keys(map)) {
            target = target.replace(new RegExp('{{ ' + key + ' }}', 'g'), map[key]);
        }
        return target;
    }

    createEl(tagName, options) {
        return document.createElement(tagName, options);
    }

    renderWithTr(trJson, targetEl) {
        let fakeRoot = this.createEl('div');
        fakeRoot.appendChild(targetEl);
        fakeRoot.querySelectorAll('[tr]').forEach((el) => {
            let tr = el.getAttribute('tr');
            if (tr === 'contents') {
                let trValue = trJson[el.innerHTML];
                if (trValue !== undefined) {
                    el.innerHTML = trValue;
                }
            } else if (tr.startsWith('attr')) {
                let trInfo = tr.split(':');
                let trValue = trJson[trInfo[2]];
                if (trValue !== undefined) {
                    el.setAttribute(trInfo[1], trValue);
                }
            }
        });
        return fakeRoot.firstChild.cloneNode(true);
    }

    localizeEl(targetEl, locale, trUrl, trName) {
        let self = this;
        let promiseStore = ResLoader.promiseStore();
        ResLoader.dynLoad(trName, trUrl);
        return new Promise((resolve, reject) => {
            let index = ResLoader.instIndex(trName, trUrl);
            promiseStore.get(index).then((json) => {
                self.translations[locale] = Object.assign(self.translations[locale] ? self.translations[locale] : {}, json);
                resolve(self.renderWithTr(self.translations[locale], targetEl));
            });
        });
    }
    
    static callOnceOn(el, eventName, callback) {
        return new Promise((resolve, reject) => {
            if (el.impl || el._implPromise) {
                if (!el.impl || (el.impl && !el.implRenderTimest)) {
                    el.addEventListener(eventName, function skElementWhenEvt(event) {
                        if (event.target === el) {
                            el.removeEventListener(eventName, skElementWhenEvt);
                            resolve(callback ? callback(event) : null);
                        }
                    });
                } else {
                    resolve(callback ? callback() : null);
                }
            } else {
                resolve(callback ? callback() : null);
            }
        });
    }

    static callWhenRendered(el, callback) {
        return Renderer.callOnceOn(el, 'skrender', callback);
    }

    whenElRendered(el, callback) {
        return Renderer.callWhenRendered(el, callback);
    }
}

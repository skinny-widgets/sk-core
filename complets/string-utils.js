

export const CONFIG_ATTRS_BY_CC = new Map([
	['theme', 'theme'],
	['basePath', 'base-path'],
	['tplPath', 'tpl-path'],
	['themePath', 'theme-path'],
	['lang', 'lang'],
	['styles', 'styles'],
	['useShadowRoot', 'use-shadow-root'],
	['reflective', 'reflective'],
	['genIds', 'gen-ids'],
	['rdVarRender', 'rd-var-render'],
	['rdCache', 'rd-cache'],
	['rdCacheGlobal', 'rd-cache-global'],
	['rdTplFmt', 'rd-tpl-fmt'],
	['stStore', 'st-store'],
	['logLv', 'log-lv'],
	['logT', 'log-t'],
]);

export class StringUtils {
	
	/**
	 * converts string-in-css-format to camelCase
	 * @param str
	 * @returns str
	 */
	static css2CamelCase(str) {
		return str.replace(/(?:^\w|[A-Z]|\b\w)/g, function (word, index) {
			return index === 0 ? word.toLowerCase() : word.toUpperCase();
		}).replace(/\s+/g, '').replace(/-/g, '');
	}
	
	static capitalizeFirstLetter(string) {
		return string.charAt(0).toUpperCase() + string.slice(1);
	}
	
	static camelCase2Css(str) {
		if (! StringUtils._camel2CssCache) {
			StringUtils._camel2CssCache = CONFIG_ATTRS_BY_CC;
		}
		if (! StringUtils._camel2CssCache.get(str)) {
			let arr = str.split(/(?=[A-Z])/);
			let cssStr = arr.map((item, index) => item.slice(0, 1).toLowerCase() + item.slice(1, item.length)).join("-");
			StringUtils._camel2CssCache.set(str, cssStr);
		}
		return StringUtils._camel2CssCache.get(str);
	}
}

let hash = (string) => {
	let hash = 0;
	string = string.toString();
	for (let i = 0; i < string.length; i++) {
		hash = (((hash << 5) - hash) + string.charCodeAt(i)) & 0xFFFFFFFF;
	}
	return hash;
}

let MAX_RECURSION = 3;

let object = (obj, level) => {
	if (obj && typeof obj.getTime === 'function') {
		return obj.getTime();
	}
	let result = 0;
	for (let property in obj) {
		if (Object.hasOwnProperty.call(obj, property)) {
			result += hash(property + value(obj[property], level));
		}
	}
	return result;
}

let value = (value, level) => {
	if (level && level > MAX_RECURSION) {
		return 0;
	}
	level = level ? level : 0;
	level++;
	let type = value == undefined ? undefined : typeof value;
	return MAPPER[type] ? MAPPER[type](value, level) + hash(type) : 0;
}

let MAPPER = {
	string: hash,
	number: hash,
	boolean: hash,
	object: object
}


export class ObjectUtils {
	static hashCode(val) {
		return value(val);
	}
}
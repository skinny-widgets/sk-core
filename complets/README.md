# Complets


Complets is the set of useful stuff around Web Components than may help you to fulfill frontend framework requirements.


## Structure

http/* - Http client abstraction and implementations

EventBus - Event bus abstraction and implementations

prefs - Portlet Preferences utilities

Renderer - Loads, cache and render native templates and operates with DOM

Registry - Dependency injector


## Usages

Gridy-grid project uses Complets framework for WebComponents base operations. 

https://bitbucket.org/techminded/gridy/src

Finistmart project uses Complets framework 

https://finistmart.com/
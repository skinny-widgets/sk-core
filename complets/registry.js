import { ResLoader } from "./res-loader.js";

export const inject = function(target, property, descriptor) {
    let executeFunctionByName = function (functionName, context /*, args */) {
        var args = Array.prototype.slice.call(arguments, 2);
        var namespaces = functionName.split(".");
        var func = namespaces.pop();
        for(var i = 0; i < namespaces.length; i++) {
            context = context[namespaces[i]];
        }
        return context[func].apply(context, args);
    };

    let capitalize = function(str) {
        return str.charAt(0).toUpperCase() + str.slice(1);
    };

    console.log('@annotated, target: ', target, ', property:',
        property, ', descriptor:', descriptor);

    descriptor.get = () => {
        let registry = window['compRegistry'];
        if (registry !== undefined) {
            return registry.getDep(capitalize(property));
        } else {
            return executeFunctionByName(capitalize(property));
        }
    }

};

export class Registry {


    constructor() {
        this.registry = {};
    }

    getDep(depName) {
        return this.registry[depName];
    }

    /**
     * If you use shared registry beaware deps with same name are not overriding by default, use ov param for that
     * example:
     *
     * window.fmRegistry = window.fmRegistry || new Registry();
     fmRegistry.wire({
        Renderer,
        catalogTr: { val: { foo: 'bar' }},
        FmProductGrid: { def: FmProductGrid, deps: {
                'renderer': Renderer
            }, is: 'fm-product-grid'}
        });

     * @param definitionName
     * @param map
     * @returns {*}
     */
    initDef(definitionName, map) {
        if (!this.registry[definitionName] || map[definitionName].ov) {
            if (map[definitionName] && map[definitionName].defName) { // definition
                let def = (Function('return ' + map[definitionName].defName))();
                if (map[definitionName].is) {
                    this.registry[definitionName] = def;
                } else {
                    this.registry[definitionName] = new def();
                }
            } else if (map[definitionName] && map[definitionName].def) { // definition
                if (map[definitionName].is) {
                    this.registry[definitionName] = map[definitionName].def;
                } else {
                    this.registry[definitionName] = new map[definitionName].def();
                }
            } else if (map[definitionName] && map[definitionName].val !== undefined && map[definitionName].val !== null) {
                this.registry[definitionName] = map[definitionName].val;
            } else if (map[definitionName] && map[definitionName].f) { // explicit factory
                if (typeof new map[definitionName].f === 'function') {
                    this.registry[definitionName] = map[definitionName].f.call(map[definitionName].f);
                } else {
                    console.error('factory must be a function');
                }
            } else { // function or constructor
                if (typeof map[definitionName] === 'function') { // guess constructor
                    try {
                        this.registry[definitionName] = new map[definitionName]();
                    } catch { // if not constructor then factory
                        console.warn('constructor not provided for ' + definitionName + ', guess this is a factory');
                        this.registry[definitionName] = map[definitionName].call(map[definitionName]);
                    }
                } else { // just assign the link
                    this.registry[definitionName] = map[definitionName];
                }
            }
        }
        return this.registry[definitionName];
    }

    wireDef(definitionName, definition, map) {
        this.initDef(definitionName, map);

        if (definition.deps) {
            let depDefs = Object.keys(definition.deps);
            for (let propName of depDefs) {
                let propDef = definition.deps[propName];
                let defName = typeof propDef === 'string' ? propDef : propDef.name;
                let dependency = this.registry[defName];
                if (! dependency) {
                    dependency = this.registry[propDef.name] = this.wireDef(defName, map);
                    if (! dependency && propDef) { // dependency not found in registry, probably inline value
                        dependency = propDef;
                    }
                }
                if (definition.is) {
                    Object.defineProperty(this.registry[definitionName].prototype, propName, { value: dependency, writable: true });
                } else {
                    Object.defineProperty(this.registry[definitionName], propName, { value: dependency, writable: true });
                }
            }
        }
        if (definition.bootstrap) {
            definition.bootstrap(this.registry[definitionName]);
        }
        if (definition.is) {
            if (! window.customElements.get(definition.is)) {
                window.customElements.define(definition.is, this.registry[definitionName]);
            }
        }
    }

    checkAndWire(definitionName, definition, map) {
        if (! map[definitionName]) {
            try {
                let def = (Function('return ' + definitionName))();
                if (def) {
                    this.wireDef(definitionName, definition, map);
                } else {
                    console.warn(definitionName + ' definition not loaded for wiring');
                }
            } catch {
                console.warn(`dependency ${definitionName} not found`);
            }
        } else {
            this.wireDef(definitionName, definition, map);
        }
    }

    wire(map, dynImportOff) {
        for (let definitionName of Object.keys(map)) {

            let definition = map[definitionName];

            if (definition.path) {
                if (! window[definitionName] || definition.forceLoad) {
                    ResLoader.loadClass(definitionName, definition.path, function(m) {
                        window[definitionName] = m[definitionName];
                        this.checkAndWire(definitionName, definition, map);
                    }.bind(this), false, dynImportOff);
                } else { // definition already loaded
                    this.checkAndWire(definitionName, definition, map);
                }
            } else {
                this.checkAndWire(definitionName, definition, map);
            }
        }
    }

    dynamicImportSupported() {
        try {
            new Function('import("")');
            return true;
        } catch (err) {
            return false;
        }
    }
}
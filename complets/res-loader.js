
export class ResLoader {

    static dynamicImportSupported() {
        try {
            new Function('import("")');
            return true;
        } catch (err) {
            return false;
        }
    }

    static isInIE() {
        let ua = window.navigator.userAgent;

        let msie = ua.indexOf('MSIE ');
        if (msie > 0) {
            return parseInt(ua.substring(msie + 5, ua.indexOf('.', msie)), 10);
        }

        let trident = ua.indexOf('Trident/');
        if (trident > 0) {
            let rv = ua.indexOf('rv:');
            return parseInt(ua.substring(rv + 3, ua.indexOf('.', rv)), 10);
        }

        let edge = ua.indexOf('Edge/');
        if (edge > 0) {
            return parseInt(ua.substring(edge + 5, ua.indexOf('.', edge)), 10);
        }

        return false;
    }

    static loadJson(url) {
        return new Promise((resolve, reject) => {
            fetch(url).then(function (response) {
                response.json().then((json) => {
                    resolve(json);
                });
            });
        });
    }

    static loadClass(className, path, callback, nowindow = false, dynImportOff = false) {
        let loaded;
        let setToWin = () => {
            if (! nowindow) {
                loaded.then(function(m) {
                    if (m[className]) {
                        window[className] = m[className];
                        return m;
                    }
                });
            }
        };
        let applyCallback = () => {
            if (callback) {
                loaded.then(callback);
            }
        };
        if (! dynImportOff && ResLoader.dynamicImportSupported() && ! ResLoader.isInIE()) {
            loaded = import(path);
            setToWin();
            applyCallback();
            return loaded;
        } else {
            loaded = new Promise(function(resolve, reject) {
                let script = document.createElement('script');
                script.onload = resolve;
                script.onerror = reject;
                script.async = true;
                script.src = path;
                document.body.appendChild(script);
            }.bind(this));
            setToWin();
            applyCallback();
            return loaded;
        }
    }
    
    static deepCopy(from, to, level, maxLevel = 256) {
        if (from == null || typeof from != "object") return from;
        if (from.constructor != Object && from.constructor != Array) return from;
        if (from.constructor == Date || from.constructor == RegExp || from.constructor == Function ||
            from.constructor == String || from.constructor == Number || from.constructor == Boolean)
            return new from.constructor(from);
        
        to = to || new from.constructor();
        
        if (! level) level = 0;
        level++;
        
        if (level <= maxLevel) {
            for (let name in from) {
                to[name] = ResLoader.deepCopy(from[name], null);
            }
        }
        
        return to;
    }
    
    static instIndex(className, path, factory) {
        return factory ? className + '::' + path + '::' +
            (factory).toString().replace(/(\r\n|\n|\r| |\t)/gm, "") : className + '::' + path;
    }
    
    static promiseStore() {
        let resLoader = window.ResLoader ? window.ResLoader : ResLoader;
        if (! resLoader._loadPromises) {
            resLoader._loadPromises = new Map();
        }
        return resLoader._loadPromises;
    }
    
    static instanceStore() {
        let resLoader = window.ResLoader ? window.ResLoader : ResLoader;
        if (! resLoader._loadInstances) {
            resLoader._loadInstances = new Map();
        }
        return resLoader._loadInstances;
    }
    
    static cacheClassDefs() {
        for (let def of arguments) {
            if (def.name && ! window[def.name]) {
                window[def.name] = def;
            }
        }
    }
    
    /**
     * does class and instance loading.
     * If class with the same name and path was previously loaded it's previous instance will be returned 
     * by default and until forceNewInstance arg is set. 
     * 
     * className - class name string
     * path - loading path
     * factory - external method to create and setup new instance
     * forceNewInstance - will do new instance with provided argument factory or class constructor. 
     * keepState - link state props from prev instance
     * deepClone - recursive copy from prev instance
     * promiseStore - use external Map for loading promises cache
     * instanceStore - use external Map for class instance cache
     * cloneImpl - use external implementation for deepClone operation 
     * dynImmportOff - turn of dynamic import use (e.g. for transpilled code)
     * sync return instance without promise, class def must be preloaded and assigned to window
     * */
    static dynLoad(className, path, factory, forceNewInstance, keepState, deepClone, promiseStore, instanceStore, cloneImpl, dynImportOff, sync, stDef) {
        let index = ResLoader.instIndex(className, path, factory);
        let promises;
        let instances;
        let doNewInstance = function(model) {
            let instance;
            if (factory) {
                instance = factory(model.constructor);
            } else {    
                instance  = new model.constructor();
            }
            if (keepState) {
                if (deepClone) {
                    if (cloneImpl && typeof cloneImpl === 'function') {
                        instance = cloneImpl(model);
                    } else {
                        ResLoader.deepCopy(model, instance);
                    }
                } else {
                    Object.assign(instance, model);
                }
            } 
            return instance;           
        }
        
        if (! promiseStore) {
            promises = ResLoader.promiseStore();
        } else {
            promises = promiseStore;
        }
        if (! instanceStore) {
            instances = ResLoader.instanceStore();
        } else {
            instances = instanceStore;
        }
        if (instances.get(index)) {
            if (forceNewInstance) {
                let model = instances.get(index);
                let instance = doNewInstance(model);
                return sync ? instance : Promise.resolve(instance);
            } else {
                return sync ? instances.get(index) : Promise.resolve(instances.get(index));
            }
        } else {
            if (sync) {
                if (stDef && ! window[className]) {
                    window[className] = stDef;
                }
                let def = (Function('return ' + className))();
                let instance;
                if (factory) {
                    instance = factory(def);
                } else {    
                    instance  = new def();
                }
                instances.set(index, instance);
                promises.set(index, Promise.resolve(instance));
                return instance;
            } else {
                if (! promises.get(index)) {
                    promises.set(index, new Promise((resolve, reject) => {
                        try {
                            if (stDef && ! window[className]) {
                                window[className] = stDef;
                            }
                            let def = (Function('return ' + className))();
                            let instance;
                            if (factory) {
                                instance = factory(def);
                            } else {    
                                instance  = new def();
                            }
                            instances.set(index, instance);
                            resolve(instance);
                        } catch {
                            if (path.endsWith(".json")) {
                                promises.set(index, ResLoader.loadJson(path));
                                promises.get(index).then((m) => {
                                    resolve(m);
                                });
                            } else {
                                promises.set(index, ResLoader.loadClass(className, path, null, false, dynImportOff));
                                promises.get(index).then((m) => {
                                    let def = typeof m[className] === 'function' ? m[className] : window[className];
                                    if (def) {
                                        let instance;
                                        if (factory) {
                                            instance = factory(def);
                                        } else {
                                            instance  = new def();
                                        }
                                        instances.set(index, instance);
                                        resolve(instance);
                                    } else {
                                        reject(`expected constructor for ${className} not found`);
                                    }
                                });
                            }

                        }
                    }));
                }
                if (forceNewInstance) {
                    return new Promise((resolve, reject) => {
                        let onLoaded = promises.get(index);
                        onLoaded.then((model) => {
                            let instance = doNewInstance(model);
                            if (! instances.get(index)) {
                                instances.set(index, instance);
                            }
                            return resolve(instance);
                        });
                    });
                } else {
                    return promises.get(index);
                }
            }
        }
    }
}

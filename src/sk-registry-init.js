import { SkRegistry } from './sk-registry.js';


export const AUTOREG_ATTR_NAME = 'sk-auto-reg';
export const AUTOREG_TAG_NAME = 'sk-registry';

export const regSkRegistry = function(attrName = AUTOREG_ATTR_NAME, tag = AUTOREG_TAG_NAME, regCl = SkRegistry) {

    if (document && (document.body && document.body.classList.contains(attrName))
        || (document.documentElement && document.documentElement.classList.contains(attrName))) {

        customElements.define(tag, regCl);
    }

}

regSkRegistry();
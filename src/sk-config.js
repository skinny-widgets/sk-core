import { Renderer } from "../complets/renderer.js";
import { ConsoleLogger } from "../complets/log.js";


export var EL_SHARED_ATTRS = {
    'theme': 'theme', 'base-path': 'basePath', 'use-shadow-root': 'useShadowRoot',
    'lang': 'lang', 'reflective': 'reflective', 'gen-ids': 'genIds', 'st-store': 'stateStoreBackend',
    'styles': 'themeStyles', 'log-lv': 'logLevel', 'tpl-vars': 'tplVars'
};


import { bindAttrPlugins, ATTR_PLUGINS_AN } from "./sk-element.js";

export const SK_CONFIG_TN = "sk-config";

export const DIMPORTS_AN = "dimports";

export class SkConfig extends HTMLElement {

    get renderer() {
        return this._renderer || null;
    }

    set renderer(renderer) {
        this._renderer = renderer;
    }

    get styles() {
        if (! this._styles) {
            this._styles = this.getAttribute('styles') ? JSON.parse(this.getAttribute('styles')) : [];
        }
        return this._styles;
    }
    
    get plugins() {
        if (! this._plugins) {
            this._plugins = [];
        }
        if (this.constructor.plugins && this.constructor.plugins.length > 0) {
            return this._plugins.concat(this.constructor.plugins);
        } else {
            return this._plugins
        }
    }

    set plugins(plugins) {
        this._plugins = plugins;
    }

    get logger() {
        if (! this._logger) {
            this._logger = ConsoleLogger.instance();
        }
    }

    set logger(logger) {
        this._logger = logger;
    }

    configureElement(el) {
        for (let attrName of Object.keys(EL_SHARED_ATTRS)) {
            let value = el.getAttribute(attrName);
            if (value === null && this.getAttribute(attrName) !== null) {
                el.setAttribute(attrName, this.getAttribute(attrName));
            }
        }
    }

    reconfigureElement(el, detail) {
        if (detail) {
            this.log(`el attribute ${detail.name} changed from: ${detail.oldValue} to: ${detail.newValue}`);
            el.setAttribute(detail.name, detail.newValue);
        } else {
            for (let attrName of Object.keys(EL_SHARED_ATTRS)) {
                let value = el.getAttribute(attrName);
                if (this.getAttribute(attrName) !== null && value !== this.getAttribute(attrName)) {
                    el.setAttribute(attrName, this.getAttribute(attrName));
                }
            }
        }
    }

    attributeChangedCallback(name, oldValue, newValue) {
        if (EL_SHARED_ATTRS[name] && oldValue !== newValue) {
            if (newValue) {
                this.dispatchEvent(
                    new CustomEvent('configChanged',
                        {detail: {name: name, oldValue: oldValue, newValue: newValue}}
                    ));
                this.dispatchEvent(
                    new CustomEvent('skconfigchanged',
                        {detail: {name: name, oldValue: oldValue, newValue: newValue}}
                    ));
            }
        }
    }

    static get observedAttributes() {
        return Object.keys(EL_SHARED_ATTRS);
    }
    
    bindAttrPlugins() {
        bindAttrPlugins.call(this, ...arguments);
    }

    setup() {
        Renderer.configureForElement(this);
        if (this.hasAttribute(ATTR_PLUGINS_AN)) {
            this.bindAttrPlugins();
        }
    }

    connectedCallback() {
        this.setup();
    }
}

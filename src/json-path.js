
//import * as jsonpath from '../../sk-jsonpath/src/jsonpath.js';

import { ConsoleLogger } from "../complets/log.js";
import { ResLoader } from "../complets/res-loader.js";

import { JsonPathPlusDriver } from "./impl/jsonpath/jsonpath-plus-driver.js";

let JSONPath;

export const JSONPATH_CN = "JsonPath";
export const JSONPATH_PT = "/node_modules/sk-core/src/json-path.js";

if (window.hasOwnProperty('jsonpath')) {
    if (typeof window.jsonpath.JSONPath === 'function') {
        JSONPath = new window.jsonpath.JSONPath();
    }
}

let logger = ConsoleLogger.instance();


export class SkJsonPathDriver {
    
    get libPath() {
        return window.skJsonPathUrl ? window.skJsonPathUrl : '/node_modules/sk-jsonpath/src/jsonpath.js';
    }
    
    lib() {
        if (! this._lib) {
            if (window.hasOwnProperty('jsonpath')) {
                this._lib = typeof window.jsonpath.JSONPath === 'function'
                    ? window.jsonpath.JSONPath() : null;

            } else {
                this.libLoadPromise = new Promise(function(resolve, reject) {
                    let onLoaded = ResLoader.loadClass('JSONPath', this.libPath, null, false, true);
                    onLoaded.then(() => {
                        if (window.hasOwnProperty('jsonpath')) {
                            this._lib = new window.jsonpath.JSONPath();
                            resolve(this._lib);
                        } else {
                            reject('Unable to load sk-jsonpath library');
                        }
                    });
                }.bind(this));
            }
        }
        return this._lib;
    }
    
    query() {
        let args = arguments;
        let impl = null;
        if (this._lib && typeof this._lib.query === 'function') {
            impl = this._lib;
        }
        if (! impl) {
            if (! this.notLoadedWarnShown) {
                logger.warn('JSONPath lib not inited, using fallback impl');
                this.notLoadedWarnShown = true;
            }
            let pathString = args[1] ? args[1].replace('$.', '') : ''; 
            // fallback simple recursive path drill impl
            let data = args[0];
            let path = pathString.split('.');
            if (path.length == 1) {
                return [data[path[0]]];
            } else {
                let item = data;
                for (let token of path) {
                    item = item[token];
                }
                return item;
            }
        } else {
            return impl.query(...args);
        }
    }
}

export class JsonPath {

    get driClass() {
        return window.JsonPathPlusEsmDriver ? // esm driver bundled at compile time
            window.JsonPathPlusEsmDriver :
            (window.JSONPath && window.JSONPath.JSONPath) ? // jsonpath-plus library pluged statically at runtime
                    JsonPathPlusDriver :
                        SkJsonPathDriver; //fallback
    }
    
    get dri() {
        if (! this._dri) {
            this._dri = new this.driClass();
        }
        return this._dri;
    }
    
    set dri(dri) {
        this._dri = dri;
    }
    
    query() {
        return this.dri.query(...arguments);
    }
}


import { SkConfig } from './sk-config.js';
import { SkApp } from './sk-app.js';
import { SkAlert } from './sk-alert.js';
import { SkButton } from './sk-button.js';
import { SkCheckbox } from './sk-checkbox.js';
import { SkDatePicker } from './sk-datepicker.js';
import { SkDialog } from './sk-dialog.js';
import { SkTab } from './sk-tab.js';
import { SkTabs } from './sk-tabs.js';
import { SkAccordion } from './sk-accordion.js';
import { SkForm } from './form/sk-form.js';
import { SkSpinner } from './sk-spinner.js';
import { SkInput } from './sk-input.js';
import { SkSelect } from './sk-select.js';
import { SkSwitch } from './sk-switch.js';
import { SkTree } from './sk-tree.js';
import { SkNavbar } from './sk-navbar.js';
import { SkRegistry } from './sk-registry.js';

export var SK_ELEMENTS = {
    'sk-config': SkConfig,
    'sk-app': SkApp,
    'sk-alert': SkAlert,
    'sk-button': SkButton,
    'sk-datepicker': SkDatePicker,
    'sk-checkbox': SkCheckbox,
    'sk-dialog': SkDialog,
    'sk-tabs': SkTabs,
    'sk-accordion': SkAccordion,
    'sk-form': SkForm,
    'sk-spinner': SkSpinner,
    'sk-input': SkInput,
    'sk-select': SkSelect,
    'sk-switch': SkSwitch,
    'sk-tree': SkTree,
    'sk-navbar': SkNavbar,
    'sk-tab': SkTab,
    'sk-registry': SkRegistry
};

export function skRegisterElements(elements) {
    let ce = window.customElements;
    for (let tag of Object.keys(elements)) {
        if (!ce.get(tag)) {
            ce.define(tag, elements[tag]);
        }
    }
}

export function skPreloadTemplates(theme) {
    return fetch('/node_modules/skinny-widgets/dist/' + theme + '.templates.html')
        .then(response => response.text())
        .then(text => {
            document.body.insertAdjacentHTML('beforeend', text);
        });
}

export async function skBootstrap(elements) {
    console.debug('found autoload enabling triggers, start bootstrapping sk components');
    console.debug('end bootstrapping sk componnents');
    let themeMatch = (document.documentElement && document.documentElement.className.match(/sk-theme-(\w+)/))
        || (document.body && document.body.className.match(/sk-theme-(\w+)/));
    if (themeMatch !== null) {
        let theme = themeMatch[1];
        let preLoadTemplates = (! (document.documentElement && document.documentElement.classList.contains('sk-auto-notpl'))
            && ! (document.body && document.body.classList.contains('sk-auto-notpl'))) && theme;
        if (theme && preLoadTemplates) {
            console.debug('preloading theme', theme);
            let tplLoad = await skPreloadTemplates(theme);
            skRegisterElements(elements);
        } else {
            skRegisterElements(elements);
        }
    } else {
        skRegisterElements(elements);
    }
}

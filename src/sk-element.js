
import { Renderer } from "../complets/renderer.js";
import { SkThemes } from "./sk-themes.js";
import { DIMPORTS_AN, EL_SHARED_ATTRS, SK_CONFIG_TN } from "./sk-config.js";
import { SkComponentImpl } from "./impl/sk-component-impl.js";

import { ResLoader } from "../complets/res-loader.js";

import { SkLocale } from "./sk-locale.js";

ResLoader.cacheClassDefs(SkLocale);

import { SkAttrChangeEvent } from "./event/sk-attr-change-event.js";

import { JsonPath, JSONPATH_CN, JSONPATH_PT } from "./json-path.js";

import { ConsoleLogger, MemoryLogger, SessionLogger, LOG_LEVEL_DEFAULT } from "../complets/log.js";
import { SK_RENDER_EVT, SkRenderEvent } from "./event/sk-render-event.js";
import { StringUtils } from "../complets/string-utils.js";
import { ObjectUtils } from "../complets/object-utils.js";

export const SK_RENDERED_EVT = "skrendered";

export const CONFIG_EL_REF_NAME = "configEl";
export const CONFIG_OBJ_REF_NAME = "skConfig";

export const DISABLED_AN = "disabled";
export const SELECTED_AN = "selected";

export const SK_CONFIG_EL_GLOBAL = "skConfigElGlobal";
export const SK_CONFIG_EL_INDIVIDUAL = "skConfigElIndividual";
export const SK_CONFIG_DISABLED = "skConfigElDisabled";

export const JSONPATH_DRI_CN_AN = "jsonpath-dri-cn";
export const JSONPATH_DRI_PATH_AN = "jsonpath-dri-path";

export const ATTR_PLUGINS_AN = "attr-plugins";

export const BASE_PATH_AN = "base-path";
export const BASE_PATH_DEFAULT = "/node_modules/skinny-widgets/src";
export const TPL_PATH_AN = "tpl-path";
export const IMPL_PATH_AN = "impl-path";
export const TPL_VARS_AN = "tpl-vars";
export const THEME_AN = "theme";
export const THEME_DEFAULT = "default";
export const LANG_AN = "lang";
export const LANG_DEFAULT = "en_US";
export const LOG_LEVEL_AN = "log-lv";
export const LOG_TARGET_AN = "log-t";
export const CONFIG_SL_AN = "config-sl";
export const STATE_STORE_AN = "st-store";
export const STATE_AN = "state";
export const RD_NOT_SHARED_AN = "rd-no-shrd";
export const SK_CONFIG_EL_GLOBAL_CN = "sk-config-el-global";
export const SK_CONFIG_EL_INDIVIDUAL_CN = "sk-config-el-individual";
export const USE_SHADOW_ROOT_AN = "use-shadow-root";
export const REFLECTIVE_AN = "reflective";
export const I18N_AN = "i18n";
export const NO_ATTR_EVTS_AN = "no-attr-evts";
export const STORED_CFG_AN = "stored-cfg";
export const STORED_PROPS_AN = "stored-props";

export function bindAttrPlugins(target) {
    let self = target ? target : this;
    let targetPlugins = target ? target.plugins : this.plugins;
    let plugins = new Map();
    for (let attr of self.attributes) {
        let cnRes = attr.name.match(/plg-(.*)-cn/);
        if (cnRes) {
            let pluginName = cnRes[1];
            if (! plugins.get(pluginName)) {
                plugins.set(pluginName, {});
            }
            plugins.get(pluginName).className = self.getAttribute(attr.name);
        } 
        let pathRes = attr.name.match(/plg-(.*)-path/);
        if (pathRes) {
            let pluginName = pathRes[1];
            if (! plugins.get(pluginName)) {
                plugins.set(pluginName, {});
            }
            plugins.get(pluginName).path = self.getAttribute(attr.name);
        }
    }
    for (let [name, plugin] of plugins) {
        if (plugin.className && plugin.path) {
            let dimportOff = self.getAttribute(DIMPORTS_AN) === 'false';
            ResLoader.dynLoad(plugin.className, plugin.path, function(def) {
                targetPlugins.push(new def(target));
            }.bind(target), false, false, false, false, false, false, dimportOff);
        }
    }
}


export class SkElement extends HTMLElement {

    get impl() {
        if (! this._impl) {
            class StubImpl extends SkComponentImpl {
            }
            this._impl = new StubImpl(this);
        }
        return this._impl;
    }

    set impl(impl) {
        this._impl = impl;
    }

    get theme() {
        return this.getAttribute(THEME_AN) || THEME_DEFAULT;
    }

    set theme(theme) {
        return this.setAttribute(THEME_AN, theme);
    }

    get basePath() {
        // defaulting for packaged library usage
        return this.getAttribute(BASE_PATH_AN) || BASE_PATH_DEFAULT;
    }

    set basePath(basePath) {
        return this.setAttribute(BASE_PATH_AN, basePath);
    }

    get tplPath() {
        return this.getAttribute(TPL_PATH_AN);
    }

    set tplPath(tplPath) {
        return this.setAttribute(TPL_PATH_AN, tplPath);
    }

    get useShadowRoot() {
        if (this.getAttribute(USE_SHADOW_ROOT_AN) === 'false') {
            return false;
        }
        return true;
    }

    get lang() {
        return this.getAttribute(LANG_AN) || LANG_DEFAULT;
    }

    get locale() {
        if (! this._locale) {
            if (this.configEl) {
                if (this.configEl.locale) {
                    if (this.lang === this.configEl.locale.lang) {
                        this._locale = this.configEl.locale;
                    } else {
                        this._locale = ResLoader.dynLoad("SkLocale", "./sk-locale.js", function(def) {
                            return new def(this.lang);
                        }.bind(this), false, null , null, null, null,
                            null, null, true, window["ResLoader"]);
                    }
                } else {
                    if (this.lang === this.configEl.lang) {
                        this._locale = ResLoader.dynLoad("SkLocale", "./sk-locale.js", function(def) {
                                return new def(this.lang);
                            }.bind(this), false, null , null, null, null,
                            null, null, true, window["ResLoader"]);
                        this.configEl.locale = this.locale;
                    } else {
                        this._locale = ResLoader.dynLoad("SkLocale", "./sk-locale.js", function(def) {
                                return new def(this.lang);
                            }.bind(this), false, null , null, null, null,
                            null, null, true, window["ResLoader"]);
                    }
                }
            } else {
                this._locale = ResLoader.dynLoad("SkLocale", "./sk-locale.js", function(def) {
                        return new def(this.lang);
                    }.bind(this), false, null , null, null, null,
                    null, null, true, window["ResLoader"]);
            }
        }
        return this._locale;
    }

    set locale(locale) {
        this._locale = locale;
    }

    get reflective() {
        return this.getAttribute(REFLECTIVE_AN) || true;
    }

    get el() {
        return this.useShadowRoot ? this.shadowRoot : this;
    }

    /**
     * copied to impl and used in SkCommonImpl.renderWithVars method
     * @returns {{}}
     */
    get tplVars() {
        if (! this._tplVars) {
            this._tplVars = this.hasAttribute(TPL_VARS_AN) ? JSON.parse(this.getAttribute(TPL_VARS_AN)) : {};
        }
        return this._tplVars;
    }

    set tplVars(tplVars) {
        this._tplVars = tplVars;
    }

    /**
     * copied to impl and used in SkCommonImpl.renderWithVars method
     * @returns {{}}
     */
    get i18n() {
        if (! this._i18n) {
            this._i18n = {};
            try {
                if (this.configEl) {
                    if (this.configEl.i18n) {
                        Object.assign(this._i18n, this.configEl.i18n);
                    } else if (this.configEl.hasAttribute(I18N_AN)) { // sk-element exists by not registred
                        Object.assign(this._i18n, JSON.parse(this.configEl.i18n));
                    }
                }
            } catch (e) {
                this.logger.error('error loading localization from sk-config element');
            }
            try {
                if (this.hasAttribute(I18N_AN)) {
                    Object.assign(this._i18n, JSON.parse(this.getAttribute(I18N_AN)));
                }
            } catch (e) {
                this.logger.error(`error loading ${this.constructor.name} element\'s own localization`);
            }
            if (this.renderer && this.renderer.translations && typeof this.renderer.translations[this.lang] === 'object') {
                Object.assign(this._i18n, this.renderer.translations[this.lang]);
            }
        }
        return this._i18n;
    }

    set i18n(i18n) {
        this._i18n = i18n;
    }


    get implClassName() {
        return this.capitalizeFirstLetter(this.theme) + 'Sk' + this.capitalizeFirstLetter(this.cnSuffix);
    }

    get implPath() {
        return this.getAttribute(IMPL_PATH_AN) ||
            `/node_modules/sk-${this.cnSuffix}-${this.theme}/src/${this.theme}-sk-${this.cnSuffix}.js`;
    }
    
    get disabled() {
        return this.hasAttribute(DISABLED_AN);
    }
    
    set disabled(disabled) {
        if (disabled) {
            this.setAttribute(DISABLED_AN, '');
        } else {
            this.removeAttribute(DISABLED_AN);
        }
    }

    get logLevels() {
        return this.hasAttribute(LOG_LEVEL_AN) ? this.getAttribute(LOG_LEVEL_AN) :
            this.hasAttribute('log_lv') ? this.getAttribute('log_lv') : LOG_LEVEL_DEFAULT;
    }

    set logLevels(level) {
        this.setAttribute(LOG_LEVEL_AN, level);
    }

    confValOrDefault(attrName, defaultVal) {
        if (this.hasAttribute(attrName)) {
            return this.getAttribute(attrName);
        } else if (this.configEl && this.configEl.hasAttribute(attrName)) {
            return this.configEl.getAttribute(attrName);
        } else {
            return defaultVal;
        }
    }
    
    confValEnabled(attrName) {
        let result = false;
        if (this.hasAttribute(attrName)) {
            result = this.getAttribute(attrName);
        } else if (this.configEl && this.configEl.hasAttribute(attrName)) {
            result = this.configEl.getAttribute(attrName);
        } else {
            result = false;
        }
        return (result !== false && result !== null && result !== undefined
                && result !== "false" && result !== "no" && result !== "null");
    }

    createLogger(el) {
        let logger;
        if (el.hasAttribute(LOG_TARGET_AN)) {
            let logTarget = el.getAttribute(LOG_TARGET_AN);
            if (logTarget === 'memory') {
                logger = new MemoryLogger();
            } else if (logTarget === 'session') {
                logger = new SessionLogger();
            } else {
                logger = new ConsoleLogger();
            }
        } else {
            logger = new ConsoleLogger();
        }
        return logger;
    }

    get logger() {
        if (! this._logger) {
            if (! this.hasAttribute(LOG_LEVEL_AN)) {
                if (this.configEl && this.configEl.logger) {
                    this._logger = this.configEl.logger;
                } else {
                    if (this.configEl) {
                        this._logger = this.createLogger(this.configEl);
                        this._logger.levels = this.logLevels = this.configEl.hasAttribute(LOG_LEVEL_AN)
                            ? this.configEl.getAttribute(LOG_LEVEL_AN) : LOG_LEVEL_DEFAULT;
                    } else {
                        this._logger = ConsoleLogger.instance();
                    }
                }
            } else {
                this._logger = this.createLogger(this);
                this._logger.levels = this.logLevels = this.getAttribute(LOG_LEVEL_AN);
            }
            if (this.configEl && ! this.configEl.logger) {
                if ((this.configEl.getAttribute(LOG_LEVEL_AN) === this.logLevels)
                    || (! this.configEl.hasAttribute(LOG_LEVEL_AN)) && this.logLevels === LOG_LEVEL_DEFAULT) {
                    this.configEl.logger = this._logger;
                }
            }
        }
        return this._logger;
    }

    set logger(log) {
        this._logger = logger;
    }

    dynamicImportSupported() {
        try {
            new Function('import("")');
            return true;
        } catch (err) {
            return false;
        }
    }

    isInIE() {
        let ua = window.navigator.userAgent;

        let msie = ua.indexOf('MSIE ');
        if (msie > 0) {
            return parseInt(ua.substring(msie + 5, ua.indexOf('.', msie)), 10);
        }

        let trident = ua.indexOf('Trident/');
        if (trident > 0) {
            let rv = ua.indexOf('rv:');
            return parseInt(ua.substring(rv + 3, ua.indexOf('.', rv)), 10);
        }

        let edge = ua.indexOf('Edge/');
        if (edge > 0) {
            return parseInt(ua.substring(edge + 5, ua.indexOf('.', edge)), 10);
        }

        return false;
    }

    initImpl() {
        try {
            let def = (Function('return ' + this.implClassName))();
            this._impl = new def(this);
        } catch {
            this._implPromise = ResLoader.loadClass(this.implClassName, this.implPath);
            this._implPromise.then((m) => {
                if (typeof m[this.implClassName] === 'function') {
                    this._impl = new m[this.implClassName](this);
                    this._impl.renderImpl();
                } else {
                    this.logger.warn(`expected constructor for ${this.implClassName} not found`);
                }
            })
        }
    }

    get stateStoreBackend() {
        return this.getAttribute(STATE_STORE_AN) || false;
    }

    set contentsState(state) {
        if (! this.stateStoreBackend) {
            this.setAttribute(STATE_AN, JSON.stringify(state));
        } else if (this.stateStoreBackend === 'session') {
            this.cid = this.hasAttribute('id') ?
                this.getAttribute('id') : null;
            if (!this.cid) {
                this.cid = this.renderer.genElId(this);
            }
            sessionStorage.setItem(this.cid, JSON.stringify(state));
        }
    }

    get contentsState() {
        if (! this.stateStoreBackend) {
            return JSON.parse(this.getAttribute(STATE_AN));
        } else if (this.stateStoreBackend === 'session') {
            return JSON.parse(sessionStorage.getItem(this.cid));
        }
    }

    get configSl() {
        return this.getAttribute(CONFIG_SL_AN) || this.getAttribute('configSl') || SK_CONFIG_TN;
    }

    get configEl() {
        if (! this._configEl) {
            if (! this.hasAttribute(CONFIG_SL_AN) && ! this.hasAttribute('configSl')) {
                if (window[CONFIG_EL_REF_NAME]) {
                    this._configEl = window[CONFIG_EL_REF_NAME];
                } else {
                    let configEl = document.querySelector(this.configSl);
                    if (configEl !== null) {
                        this._configEl = configEl;
                        window[CONFIG_EL_REF_NAME] = this._configEl;
                    }
                }
            } else {
                this._configEl = document.querySelector(this.configSl);
            }
        }
        return this._configEl;
    }

    set configEl(configEl) {
        this._configEl = configEl;
    }

    get subEls() {
        return [];
    }

    get renderer() {
        return this._renderer;
    }

    set renderer(renderer) {
        this._renderer = renderer;
    }

    get skThemeLoader() {
        if (this.configEl && this.configEl.skTheme) {
            return Promise.resolve(this.configEl.skTheme);
        } else {
            if (this.configEl) {
                this.configEl.skThemeLoader = SkThemes.byName(this.theme, this.configEl || this);
                return this.configEl.skThemeLoader;
            } else {
                return SkThemes.byName(this.theme, this.configEl || this);
            }
        }
    }

    get validationMessage() {
        return this._validationMessage || this.locale.tr('Field value is invalid');
    }

    set validationMessage(validationMessage) {
        this._validationMessage = validationMessage;
    }

    get connections() {
        if (! this._connections) {
            this._connections = this.constructor.connections || [];
        }
        return this._connections;
    }

    set connections(connections) {
        this._connections = connections;
    }

    get jsonPath() {
        if (! this._jsonPath) {
            this._jsonPath = ResLoader.dynLoad(JSONPATH_CN, JSONPATH_PT, function(def) {
                return new def();
            }.bind(this), false, false, false ,false,
                false, false, true, true, JsonPath);
        }
        return this._jsonPath;
    }

    set jsonPath(jsonPath) {
        this._jsonPath = jsonPath;
    }
    
    get storedCfg() {
        if (! this._storedCfg) {
            if (this.hasAttribute(STORED_CFG_AN)) {
                try {
                    this._storedCfg = JSON.parse(this.getAttribute(STORED_CFG_AN));
                } catch (e) {
                    this.logger.warn("Error parsing stored-cfg for element", this);
                }
            }
            if (! this._storedCfg) {
                this._storedCfg = {};
            }
        }
        return this._storedCfg;
    }
    
    set storedCfg(storedCfg) {
        this._storedCfg = storedCfg;
        this.setAttribute(STORED_CFG_AN, JSON.stringify(this._storedCfg));
    }

    toBool(s) {
        let regex=/^\s*(true|1|on)\s*$/i;
        return regex.test(s);
    }

    applyToTarget(source, target, targetType, el, value) {
        if (targetType === 'event' || !targetType) { // event is default target type
            let event = target ? new CustomEvent(target, { detail: { value: value }}) : new Event('click');  // click is default event
            el.dispatchEvent(event);
        } else if (targetType === 'method') {
            el[target].call(el, value);
        } else if (targetType === 'attr') {
            el.setAttribute(target, value);
        } else if (targetType === 'togglattr') {
            if (value && typeof value === 'string' && value.indexOf("|") > 0) {
                let variants = value.split("|");
                if (el.getAttribute(target) === variants[0]) {
                    el.setAttribute(target, variants[1]);
                } else {
                    el.setAttribute(target, variants[0]);
                }
            } else if (value && value === 'true' || value === 'false') {
                let val = this.toBool(el.getAttribute(target));
                el.setAttribute(target, ! val);
            } else {
                if (el.hasAttribute(target)) {
                    el.removeAttribute(target);
                } else {
                    el.setAttribute(target, value);
                }
            }
        } else if (targetType === 'prop') {
            el[target] = value;
        } else if (targetType === 'togglprop') {
            let val = this.toBool(value);
            if (el[target] === val) {
                el[target] = ! val;
            } else {
                el[target] = val;
            }
        } else if (targetType === 'function') {
            target.call(el, value);
        }
        this['_' + source] = value;
    }

    findTarget(sl) {
        let el = sl ? this.querySelector(sl) || this.el ? this.el.querySelector(sl) : null : null;
        if (! el) {
            el = document.querySelector(sl);
            el = el ? el : (sl ? null : this);
        }
        return el;
    }

    attrsToJson(attrs, targetEl, css2Camel) {
        let json = {};
        for (let attr of attrs) {
            if (typeof attr === 'object') {
                if (attr.type === 'array' || attr.type === 'object') {
                    let key = css2Camel ? this.css2CamelCase(attr.name) : attr.name;
                    if (targetEl.hasAttribute(attr.name)) {
                        try {
                            json[key] = JSON.parse(targetEl.getAttribute(attr.name));
                        } catch {
                            this.logger.error('can not parse element attribute value as json');
                        }
                    }
                }
            } else {
                if (targetEl.hasAttribute(attr)) {
                    let key = css2Camel ? this.css2CamelCase(attr) : attr;
                    json[key] = targetEl.getAttribute(attr);
                }
            }
        }
        return json;
    }

    setupConnections() {
        if (this.connections.length > 0) {
            for (let connection of this.connections) {
                let { from, sourceType, source, to, targetType, target, value }  = connection;
                let fromEl = this.findTarget(from);
                if (sourceType === 'event' || ! sourceType) { // event is default source type
                    source = source ? source : 'click';  // click is default event
                    target = target ? target : 'click';
                    if (fromEl) {
                        fromEl.addEventListener(source, function (event) {
                            let toEl = this.findTarget(to);
                            this.applyToTarget(source, target, targetType, toEl, value || event.target.value || event);
                        }.bind(this));
                    } else {
                        this.logger.warn(`${from} element specified in connections not found`);
                    }
                } else if (sourceType === 'prop') {
                    let descriptor = Object.getOwnPropertyDescriptor(fromEl, source);
                    if (descriptor) {
                        Object.defineProperty(fromEl, source, {
                            set: function (value) {
                                descriptor.set(value);
                                let toEl = this.findTarget(to);
                                this.applyToTarget(source, target, targetType, toEl, value);
                            }.bind(this),
                            get: function () {
                                return this['_' + source];
                            }.bind(this),
                            configurable: true
                        });
                    } else {
                        Object.defineProperty(fromEl, source, {
                            set: function (value) {
                                this['_' + source] = value;

                                let toEl = this.findTarget(to);
                                this.applyToTarget(source, target, targetType, toEl, value);
                            }.bind(this),
                            get: function () {
                                return this['_' + source];
                            }.bind(this),
                            configurable: true
                        });
                    }
                } else if (sourceType === 'attr') {
                    let observer = new MutationObserver(function() {
                        let toEl = this.findTarget(to);
                        this.applyToTarget(source, target, targetType, toEl, value);
                    }.bind(this));
                    observer.observe(fromEl, { attributes: true });
                }
            }
        }
    }

    onReflection(event) {
        this.logger.debug('sk-element configChanged', event);
        this.configEl.reconfigureElement(this, event.detail);
        if (this.impl.contentsState) {
            this.contentsState = JSON.parse(JSON.stringify(this.impl.contentsState));
        }
        if (event.detail.name && event.detail.name === 'theme') {
            this.impl.unmountStyles();
            this.impl.unbindEvents();
            this.impl.clearTplCache();
            this.impl = null;
        }
        this.addEventListener(SK_RENDER_EVT, (event) => {
            this.impl.restoreState({ contentsState: this.contentsState });
        });
        this.render();
    }

    fromEntries(entries) {
        if (!entries || !entries[Symbol.iterator]) {
            throw new Error('SkElement.fromEntries() requires a single iterable argument');
        }
        let obj = {};
        for (let [key, value] of entries) {
            obj[key] = value;
        }
        return obj;
    }

    initJsonPath(cn, path) {
        this.jsonPath = new JsonPath();
        let dynImportsOff = (this.confValOrDefault(DIMPORTS_AN, null) === "false");
        ResLoader.dynLoad(cn, path, function(def) {
            this.jsonPath.dri =  new def();
        }.bind(this), false, false, false ,false, false, false, dynImportsOff);
    }

    setupJsonPath() {
        let jsonPathDriCn = this.confValOrDefault(JSONPATH_DRI_CN_AN, false);
        let jsonPathDriPath = this.confValOrDefault(JSONPATH_DRI_PATH_AN, false);
        if (this.hasAttribute(JSONPATH_DRI_CN_AN) && this.hasAttribute(JSONPATH_DRI_PATH_AN)) {
            this.initJsonPath(jsonPathDriCn, jsonPathDriPath);
        } else {
            if (this.configEl) {
                if (this.configEl.jsonPath) {
                    this.jsonPath = this.configEl.jsonPath;
                } else {
                    if (jsonPathDriCn && jsonPathDriPath) {
                        this.initJsonPath(jsonPathDriCn, jsonPathDriPath);
                    } else {
                        this.jsonPath;
                    }
                    this.configEl.jsonPath = this.jsonPath;
                }
            } else {
                this.jsonPath;
            }
        }
    }

    setupRenderer() {
        // if no renderer were injected, check if we can use shared by config renderer
        if (! this.renderer) {
            if (! this.hasAttribute(RD_NOT_SHARED_AN) ||
                (this.configEl && !this.configEl.hasAttribute(RD_NOT_SHARED_AN))) {
                if (this.configEl) {
                    if (! this.configEl.renderer) { // configEl exists but renderer not yet bootstrapped
                        Renderer.configureForElement(this.configEl);
                    }
                    this.renderer = this.configEl.renderer;
                } else { // no configEl, create shared renderer in window
                    if (! window.skRenderer) {
                        window.skRenderer = new Renderer();
                    }
                    this.renderer = window.skRenderer;
                }
            }
        }
        // instanciate new render if no one were provided before or element tag has render options defined
        Renderer.configureForElement(this);
    }

    skConfigElPolicy() {
        if (document.documentElement && document.documentElement.classList.contains(SK_CONFIG_EL_GLOBAL_CN)
            || (document.body && document.body.classList.contains(SK_CONFIG_EL_GLOBAL_CN))) {
            return SK_CONFIG_EL_GLOBAL;
        } else if (document.documentElement && document.documentElement.classList.contains(SK_CONFIG_EL_INDIVIDUAL_CN)
            || (document.body && document.body.classList.contains(SK_CONFIG_EL_INDIVIDUAL_CN))) {
            return SK_CONFIG_EL_INDIVIDUAL;
        } else {
            return SK_CONFIG_DISABLED;
        }
    }

    setupConfigEl() {
        if (window[CONFIG_OBJ_REF_NAME]) {
            this.config = window[CONFIG_OBJ_REF_NAME];
            let globalConfigElAbsent = false;
            if (! this.configEl) {
                globalConfigElAbsent = true;
                this.configEl = document.createElement('sk-config');
            }
            for (let propKey of Object.keys(this.config)) {
                let attrName = this.camelCase2Css(propKey);
                if (! this.configEl.hasAttribute(attrName)) {
                    this.configEl.setAttribute(attrName, this.config[propKey]);
                }
            }
            if (globalConfigElAbsent && this.skConfigElPolicy() === SK_CONFIG_EL_GLOBAL) {
                document.body.appendChild(this.configEl);
            }
        }
    }

    setup() {
        this.setupConfigEl();
        this.setupRenderer();
        this.setupJsonPath();
        let attrPlugins = this.confValOrDefault(ATTR_PLUGINS_AN, false) || this.hasAttribute(ATTR_PLUGINS_AN);
        if (attrPlugins) {
            this.bindAttrPlugins();
        }
        if (this.configEl && this.configEl.configureElement) {
            this.configEl.configureElement(this);
            if (this.reflective && this.reflective !== 'false') {
                this.configEl.addEventListener('configChanged', this.onReflection.bind(this));
            }
        } else {
            if (this.configEl) { // sk-config was not registred, use fallback configuration
                for (let attrName of Object.keys(EL_SHARED_ATTRS)) {
                    let value = this.getAttribute(attrName);
                    if (value === null && this.configEl.getAttribute(attrName) !== null) {
                        this.setAttribute(attrName, this.configEl.getAttribute(attrName));
                    }
                }
            }
            if (! this.hasAttribute(BASE_PATH_AN)) {
                this.logger.warn(this.constructor.name, " ",
                    "configEl not found or sk-config element not registred, " +
                    "you will have to add base-path and theme(if not default) attributes for each sk element");
            }
        }

        if (this.useShadowRoot) {
            if (! this.shadowRoot) {
                this.attachShadow({mode: 'open'});
            }
        }
    }
    
    withUpdatesLocked(callback) {
        if (typeof callback === 'function') {
            this.updatesLocked = true;
            callback.call(this);
            this.updatesLocked = false;
        }
    }
    
    get storedProps() {
        if (! this._storedProps) {
            if (this.hasAttribute(STORED_PROPS_AN)) {
                try {
                    this._storedProps = JSON.parse(this.getAttribute(STORED_PROPS_AN));
                } catch (e) {
                    this.logger.warn('Error parsing stored attrs attribute for element', this);
                    this._storedProps = [];
                }
            } else {
                this._storedProps = [];
            }
        }
        return this._storedProps;
    }
    
    set storedProps(props) {
        this._storedProps = props;
    }
    
    loadStoredCfg() {
        if (this.storedProps.length > 0) {
            for (let prop of this.storedProps) {
                if (this.storedCfg[prop]) {
                    this.withUpdatesLocked(() => {
                        this[prop] = this.storedCfg[prop];
                    });
                }
            }
        }
    }
    
    async render() {
        this.callPluginHook('onRenderStart');
        if (this.impl) {
            this.addToRendering();
            this.impl.renderImpl();
            //this.impl.bindEvents();
            /*        this.bindAutoRender();
                    this.callPluginHook('onRenderEnd');
                    this.dispatchEvent(new CustomEvent('skrender', { bubbles: true, composed: true }));*/
        } else {
            this.bindAutoRender();
            this.setupConnections();
            if (this.skConfigElPolicy() === SK_CONFIG_EL_INDIVIDUAL) {
                this.el.appendChild(this.configEl);
            }
            this.callPluginHook('onRenderEnd');
            this.renderTimest = Date.now();
            this.dispatchEvent(new CustomEvent('rendered', { bubbles: true, composed: true })); // :DEPRECATED
            this.dispatchEvent(new SkRenderEvent({ bubbles: true, composed: true }));
        }
    }

    css2CamelCase(str) {
        return StringUtils.css2CamelCase(str);
    }

    camelCase2Css(str) {
        return StringUtils.camelCase2Css(str)
    }

    capitalizeFirstLetter(str) {
        return StringUtils.capitalizeFirstLetter(str);
    }

    bindByCn(attrName, el, type) {
        if (attrName != null) {
            let propName = this.css2CamelCase(attrName);

            let descriptor = Object.getOwnPropertyDescriptor(this, propName);
            if (descriptor) {
                Object.defineProperty(this, propName, {
                    set: function (value) {
                        descriptor.set(value);
                        if (type === 'at') {
                            if (propName.indexOf('_') > 0) {
                                let [source, target] = propName.split('_');
                                el.setAttribute(target, value);
                            } else {
                                el.setAttribute(propName, value);
                            }
                        } else if (type === 'cb') {
                            let callbackName = 'on' + this.capitalizeFirstLetter(propName) + 'Change';
                            if (typeof this[callbackName] === 'function') {
                                this[callbackName].call(this, el, value);
                            }
                        } else {
                            el.innerHTML = value;
                        }
                        this['_' + propName] = value;
                    }.bind(this),
                    get: function () {
                        return this['_' + propName];
                    }.bind(this),
                    configurable: true
                });
            } else {
                Object.defineProperty(this, propName, {
                    set: function (value) {
                        if (type === 'at') {
                            if (propName.indexOf('_') > 0) {
                                let [source, target] = propName.split('_');
                                el.setAttribute(target, value);
                            } else {
                                el.setAttribute(propName, value);
                            }
                        } else if (type === 'cb') {
                            let callbackName = 'on' + this.capitalizeFirstLetter(propName) + 'Change';
                            if (typeof this[callbackName] === 'function') {
                                this[callbackName].call(this, el, value);
                            }
                        } else {
                            el.innerHTML = value;
                        }
                        this['_' + propName] = value;
                    }.bind(this),
                    get: function () {
                        return this['_' + propName];
                    }.bind(this),
                    configurable: true
                });
            }
        }
    }

    bindAutoRender(target) {
        target = target ? target : this.el;
        let autoRenderedEls = target.querySelectorAll('[class*="sk-prop-"]');
        for (let autoEl of autoRenderedEls) {
            // bind in and at classed elements contents and attrs to element's attributes or class props
            // if class property with name matched found we bind it
            // if class property with name matched not found we define and bind it
            let inNames = autoEl.className.match('sk-prop-in-(\\S+)');
            if (inNames) {
                // :TODO validate and transform name to oroper attr or prop name
                this.bindByCn(inNames[1], autoEl);
            }
            let atNames = autoEl.className.match('sk-prop-at-(\\S+)');
            if (atNames) {
                // :TODO validate and transform name to oroper attr or prop name
                this.bindByCn(atNames[1], autoEl, 'at');
            }
            let cbNames = autoEl.className.match('sk-prop-cb-(\\S+)');
            if (cbNames) {
                // :TODO validate and transform name to oroper attr or prop name
                this.bindByCn(cbNames[1], autoEl, 'cb');
            }
        }

    }
    
    initRendering() {
        if (! window.skRendering) {
            window.skRendering = new Set();
        }
    }

    addToRendering() {
        this.initRendering();
        window.skRendering.add(this);
    }

    removeFromRendering() {
        if (window.skRendering) {
            window.skRendering.delete(this);
            if (window.skRendering.size <= 0) {
                window.dispatchEvent(new CustomEvent(SK_RENDERED_EVT));
                window.skRendered = true;
            }
        } else {
            this.logger.warn('No rendering set found');
        }
    }

    /**
     * preload templates by specified by preLoadedTpls array property with names and matching ${name}Path properties
     * tplName must be like itemTpl, itemActiveTpl, buttonTpl, it will be injected in this class with provided name
     * tplName + 'Path' attribute must be defined in class, e.g. itemTplPath, itemActiveTplPath
     * @returns {Promise<unknown>}
     */
    async preloadTemplates() {
        return new Promise((resolve) => {
            let loadedTpls = [];
            let tplMap = new Map();
            // tplName must be like itemTpl, itemActiveTpl, buttonTpl,
            for (let tplName of this.preLoadedTpls) {
                let id = this.constructor.name + this.capitalizeFirstLetter(tplName);
                let tplLoad = this.renderer.mountTemplate(this[tplName + 'Path'], id, this);
                loadedTpls.push(tplLoad);
                tplMap.set(id, tplName);
            }
            Promise.all(loadedTpls).then((loadedTpls) => {
                // inject templates by attr name w/o path
                for (let loadedTpl of loadedTpls) {
                    let propName = tplMap.get(loadedTpl.getAttribute('id'));
                    this[propName] = loadedTpl;
                }
                resolve(loadedTpls);
            });
        });
    }

    connectedCallback() {
        this.initRendering();
        this.loadStoredCfg();
        this.setup();
        if (this.preLoadedTpls && Array.isArray(this.preLoadedTpls) && this.preLoadedTpls.length > 0) {
            this.preloadTemplates().then(() => {
                this.render();
            })
        } else {
            this.render();
        }
    }

    setCustomValidity(validity) {
        this.validationMessage = validity;
        this.impl.renderValidation(validity);
    }

    flushValidity() {
        this.impl.flushValidation();
    }

    get plugins() {
        if (! this._plugins) {
            this._plugins = [];
        }
        if (this.constructor.plugins && this.constructor.plugins.length > 0) {
            return this._plugins.concat(this.constructor.plugins);
        } else {
            return this._plugins
        }
    }

    set plugins(plugins) {
        this._plugins = plugins;
    }

    addPlugin(plugin, global = false) {
        if (global) {
            if (! this.constructor.plugins) {
                this.constructor.plugins = [];
            }
            this.constructor.plugins.push(plugin);
        } else {
            this.plugins.push(plugin);
        }
    }
    
    walkInPlugins(name, plugins, ...rest) {
        for (let plugin of plugins) {
            if (typeof plugin[name] === 'function') {
                plugin[name].call(this, ...rest);
            } else if (typeof plugin[name] === 'string') {
                let functionName = plugin[name];
                if (typeof this[functionName] === 'function') {
                    this[functionName].call(this, ...rest);
                } else if (this.impl && typeof this.impl[functionName] === 'function') {
                    this.impl[functionName].call(this, ...rest);
                } else if (typeof window[functionName] === 'function') {
                    window[functionName].call(this, ...rest);
                }
            }
        }

    }
    
    bindAttrPlugins() {
        bindAttrPlugins.call(this, ...arguments);
    }

    callPluginHook(name, ...rest) {
        if (this.configEl && Array.isArray(this.configEl.plugins) && this.configEl.plugins.length > 0) {
            this.walkInPlugins(name, this.configEl.plugins, ...rest);
        }
        if (Array.isArray(this.plugins) && this.plugins.length > 0) {
            this.walkInPlugins(name, this.plugins, ...rest);
        }
    }

    clearElCache() {
        for (let el of this.subEls) {
            if (this[el]) {
                this[el] = null;
            }
        }
    }

    whenRendered(callback) {
        return this.renderer ? this.renderer.whenElRendered(this, callback) : Renderer.callWhenRendered(this, callback);
    }
    
    get renderDeferred() {
        if (! this._renderDeferred) {
            if (this.renderer.jquery) {
                this._renderDeferred = new this.renderer.jquery.Deferred();
            } else {
                this._renderDeferred = null;
            }
        }
        return this._renderDeferred;
    }
    
    onRendered(callback) {
        if (this.renderDeferred) {
            this.renderDeferred.then((result) => {
                callback.call(this, result);
            });
        } else {
            if (! this.renderer.jquery) {
                this.logger.warn('onRendered() requires jquery to be loaded for renderer');
            }
        }
    }

    async runSerial(taskFunctions) {
        let starterPromise = Promise.resolve(null);
        await taskFunctions.reduce(
            (p, def) => {
                if (typeof def === 'function') {
                    return p.then(() => def.call(this));
                } else {
                    let args = def.args ? def.args : []
                    return p.then(() => def.func.call(this, ...args));
                }
            },
            starterPromise
        );
    }

    disconnectedCallback() {
        if (this.impl && typeof this.impl.onDisconnected === 'function') {
            this.impl.onDisconnected();
        }
    }

    attributeChangedCallback(name, oldValue, newValue) {
        this.callPluginHook('onAttrChange', name, oldValue, newValue);
        if (! this.confValOrDefault(NO_ATTR_EVTS_AN, false)) {
            this.dispatchEvent(new SkAttrChangeEvent({ detail: { name: name, oldValue: oldValue, newValue: newValue }}));
        }
    }

    focus() {
        if (this?.impl.focus && typeof this.impl.focus === 'function') {
            this.impl.focus();
        }
    }

    /**
     * translate string with placeholders in {0} and {{ var }} formats,
     * first one is chosen when more than one extra argument passed and
     * it is not an object. Initial but rendered string returned if translate
     * not found.
     * e.g. this.tr('Hello, {0}, your email is {1}', 'John', 'mail@example.com');
     * e.g. this.tr('Hello, {{ name }}, your email is {{ email }}', { username: 'John', email: 'mail@example.com' });
     * @param target
     * @param placeholders
     * @returns string
     */
    tr(target) {
        let str = this.i18n[target] ? this.i18n[target] : target;
        return (arguments.length > 1)
            ? this.format(str, ...Array.prototype.slice.call(arguments, 1)) : str;
    }

    format(str) {
        let values = arguments.length > 1 ? Array.prototype.slice.call(arguments, 1) : [];
        if (values.length === 1 && typeof values[0] === 'object') {
            return this.renderer.replaceVars(str, values[0]);
        } else {
            return str.replace(/{(\d+)}/g, function (match, number) {
                return typeof values[number] != 'undefined'
                    ? values[number]
                    : match
                    ;
            });
        }
    }

    async loadDataLink(dataLink, target) {
        let value = null;
        target = target ? target : this;
        if (typeof dataLink === 'string') {
            if (this[dataLink]) {
                if (typeof target[dataLink] === 'function') {
                    value = await target[dataLink].call(this);
                } else {
                    value = target[dataLink];
                }
            } else if (window[dataLink]) {
                if (typeof window[dataLink] === 'function') {
                    value = await window[dataLink].call(window);
                } else {
                    value = window[dataLink];
                }
            } else {
                value = this.jsonPath.query(this, dataLink)[0];
                if (value) {
                    if (typeof value === 'function') {
                        value = await value.call(this);
                    }
                } else {
                    value = this.jsonPath.query(window, dataLink)[0];
                    if (typeof value === 'function') {
                        value = await value.call(this);
                    }
                }
            }
        }
        return Promise.resolve(value);
    }
}

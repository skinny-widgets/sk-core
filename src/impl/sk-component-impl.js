
import { SK_CONFIG_EL_INDIVIDUAL } from "../sk-element.js";


/**
 * elements than can have action attribute to be binded with event or callback, currently in sk-dialog and sk-form only
 * @type {string}
 */
export var ACTION_SUBELEMENTS_SL = 'button,sk-button';

import { EventTarget } from "./event-target.js";
import { SkRenderEvent } from "../event/sk-render-event.js";

export class SkComponentImpl extends EventTarget {

/*    get tplPath() {
        if (! this._tplPath) {
            this._tplPath = this.comp.tplPath || `${this.themePath}/${this.prefix}-sk-${this.suffix}.tpl.html`;
        }
        return this._tplPath;
    }*/

    get tplPath() {
        if (! this._tplPath) {
            if (this.comp.tplPath) {
                this._tplPath = this.comp.tplPath
            } else {
                this._tplPath = (this.comp.configEl && this.comp.configEl.hasAttribute('tpl-path'))
                    ? `${this.comp.configEl.getAttribute('tpl-path')}/${this.prefix}-sk-${this.suffix}.tpl.html`
                    : `/node_modules/sk-${this.comp.cnSuffix}-${this.prefix}/src/${this.prefix}-sk-${this.suffix}.tpl.html`;
            }
        }
        return this._tplPath;
    }

    set tplPath(tplPath) {
        this.comp.setAttribute('tpl-path', tplPath);
        this.tplPath;
    }

    get themePath() {
        if (this.skTheme && this.skTheme.basePath) {
            return this.skTheme.basePath;
        } else {
            let basePath = (this.comp.basePath).toString();
            this.logger.warn('theme path is computed from basePath', basePath, this.skTheme);
            if (basePath.endsWith('/')) { // remove traling slash
                basePath = basePath.substr(0, basePath.length - 1);
            }
            return `${basePath}/theme/${this.comp.theme}`;
        }
    }

    get mountedStyles() {
        if (! this._mountedStyles) {
            this._mountedStyles = {};
        }
        return this._mountedStyles;
    }

    set mountedStyles(mountedStyles) {
        this._mountedStyles = mountedStyles;
    }

    constructor(comp) {
        super();
        this.comp = comp;
    }

    bindEvents() {}

    unbindEvents() {

    }

    dumpState() {

    }

    saveState() {
        if (! this.comp.contentsState) {
            if (this.comp.useShadowRoot) {
                this.comp.contentsState = this.comp.el.host.innerHTML;
                this.comp.el.host.innerHTML = '';
            } else {
                this.comp.contentsState = this.comp.el.innerHTML;
                this.comp.el.innerHTML = '';
            }
        }
    }

    restoreState(state) {

    }

    enable() {
    }
    disable() {
    }

    afterRendered() {
        this.comp.dispatchEvent(new CustomEvent('skafterrendered'));
    }

    beforeRendered() {
        this.comp.dispatchEvent(new CustomEvent('skbeforerendered'));
    }

    unmountStyles() {

    }

    get logger() {
        return this.comp.logger;
    }

    findStyle(seek) {
        let links = document.querySelectorAll('link');
        let matched = [];
        for (let link of links) {
            if (Array.isArray(seek)) {
                for (let seekItem of seek) {
                    if (link.href.match(seekItem)) {
                        matched.push(link);
                        break;
                    }
                }
            } else {
                if (link.href.match(seek)) {
                    matched.push(link);
                }
            }
        }
        return matched;
    }

    attachStyleByName(name, target) {
        if (! target) {
            target = this.comp.el;
        }
        let path = this.comp.configEl.styles[name];
        if (path) {
            let link = document.createElement('link');
            link.setAttribute('rel', 'stylesheet');
            link.setAttribute('href', path);
            target.appendChild(link);
        } else {
            this.logger.warn('style not found with config el', name);
        }
    }

    attachStyleByPath(path, target) {
        if (! target) {
            target = this.comp.el;
        }
        if (target.querySelector(`link[href='${path}']`) == null) {
            let link = document.createElement('link');
            link.setAttribute('rel', 'stylesheet');
            link.setAttribute('href', path);
            target.appendChild(link);
        }
    }

    genStyleLinks() {
        if (this.comp.hasAttribute('styles') && this.comp.getAttribute('styles') === 'false') {
            return false;
        }
        if (this.skTheme && this.skTheme.styles) {
            let styles = this.skTheme.styles;
            let configStyles = this.comp.configEl && this.comp.configEl.styles ? Object.keys(this.comp.configEl.styles) : [];
            for (let styleName of Object.keys(styles)) {
                let href = configStyles.includes(styleName) ? this.comp.configEl.styles[styleName] : styles[styleName];
                let fileName = this.fileNameFromUrl(href);
                if (!this.mountedStyles[fileName]) { // mount only not mounted before
                    let link = document.createElement('link');
                    link.setAttribute('rel', 'stylesheet');
                    link.setAttribute('href', href);
                    this.comp.el.appendChild(link);
                    this.mountedStyles[fileName] = link;
                    this.logger.info(`mounted style: ${fileName} as ${href} to component: ${this.comp.tagName}`);
                }
            }
        }
    }


    mountStyles() {
        if (! this.skTheme && this.comp.skThemeLoader) { // :TODO optimize this
            this.comp.skThemeLoader.then(function(skTheme) {
                this.genStyleLinks();
            }.bind(this));
        } else {
            this.genStyleLinks();
        }
    }

    mountStyle(fileNames, target) {
        if (! target) {
            target = this.comp.el;
        }
        if (this.comp.useShadowRoot) {
            if (this.comp.configEl && this.comp.configEl.styles) {
                if (Array.isArray(fileNames)) {
                    for (let name of fileNames) {
                        this.attachStyleByName(name, target);
                    }
                } else {
                    this.attachStyleByName(fileNames, target);
                }
            } else {
                let styles = this.findStyle(fileNames);
                if (styles && styles.length > 0) {
                    let style = styles[0];
                    if (style) {
                        target.appendChild(document.importNode(style));
                    } else {
                        this.logger.warn('style not found', fileNames);
                    }
                }
            }
        }
    }

    clearTplCache() {
        if (this.cachedTplId) {
            let tplEl = document.querySelector('#' + this.cachedTplId);
            if (tplEl !== null) {
                tplEl.remove();
            }
        }
    }

    get cachedTplId() {
        return this.comp.constructor.name + 'Tpl';
    }

    get skFormParent() {
        if (this._skFormParent === undefined) {
            this._skFormParent = this.findParent(this.comp, 'SK-FORM');
        }
        return this._skFormParent;
    }

    idGenEnabled() {
        return this.comp.configEl && this.comp.configEl.hasAttribute('gen-ids')
            && this.comp.configEl.hasAttribute('gen-ids') !== 'false';
    }

    isInIE() {
        let ua = window.navigator.userAgent;

        let msie = ua.indexOf('MSIE ');
        if (msie > 0) {
            return parseInt(ua.substring(msie + 5, ua.indexOf('.', msie)), 10);
        }

        let trident = ua.indexOf('Trident/');
        if (trident > 0) {
            let rv = ua.indexOf('rv:');
            return parseInt(ua.substring(rv + 3, ua.indexOf('.', rv)), 10);
        }

        let edge = ua.indexOf('Edge/');
        if (edge > 0) {
            return parseInt(ua.substring(edge + 5, ua.indexOf('.', edge)), 10);
        }

        return false;
    }

    initTheme() {
        if (! this.skTheme) {
            try {
                let cn = this.comp.capitalizeFirstLetter(this.comp.theme) + 'Theme';
                let def = (Function('return ' + cn))();
                if (def) {
                    this.skTheme = new def(this.comp.configEl);
                    if (this.comp.configEl) {
                        this.comp.configEl.skTheme = this.skTheme;
                    }
                    return Promise.resolve(this.skTheme);
                }
            } catch {
                return new Promise(function(resolve) {
                    this.comp.skThemeLoader.then(function(skTheme) {
                        if (this.comp.configEl && this.comp.configEl.skTheme) {
                            this.skTheme = this.comp.configEl.skTheme;
                            resolve(this.skTheme);
                        } else {
                            let cn = this.comp.capitalizeFirstLetter(this.comp.theme) + 'Theme';
                            let def = false;
                            if (skTheme[cn]) {
                                def = skTheme[cn];
                            } else {
                                try {
                                    def = (Function('return ' + cn))();
                                } catch {
                                    this.logger.warn('unable to load theme class');
                                }
                            }
                            if (def) {
                                this.skTheme = new def(this.comp.configEl);
                                if (this.comp.configEl) {
                                    this.comp.configEl.skTheme = this.skTheme;
                                }
                            }
                            resolve(this.skTheme);
                        }
                    }.bind(this));
                }.bind(this));
            }
        } else {
            return Promise.resolve(this.skTheme);
        }
    }

    doRender() {
        let id = this.getOrGenId();
        this.beforeRendered();
        this.renderWithVars(id);
        this.indexMountedStyles();
        this.afterRendered();
        this.bindEvents();
        this.comp.bindAutoRender();
        this.comp.setupConnections();
        if (this.comp.configEl && this.comp.skConfigElPolicy() === SK_CONFIG_EL_INDIVIDUAL) {
            this.comp.el.appendChild(this.comp.configEl);
        }
        this.comp.callPluginHook('onRenderEnd');
        this.comp.implRenderTimest = Date.now();
        this.comp.removeFromRendering();
        this.comp.dispatchEvent(new CustomEvent('rendered', { bubbles: true, composed: true })); // :DEPRECATED
        this.comp.dispatchEvent(new SkRenderEvent({ bubbles: true, composed: true }));
        if (this.comp.renderDeferred) {
            this.comp.renderDeferred.resolve(this.comp);
        }
    }

    noTplRendering() {
        if (! this.tplPath) {
            return true;
        }
        let undef = this.tplPath.match('undefined');
        if (undef !== null && undef.length > 0) {
            return true;
        }
        return false;
    }

    renderImpl() {
        if (this.noTplRendering()) {
            this.doRender();
        } else {
            let themeLoaded = this.initTheme();
            themeLoaded.finally(() => { // we just need to try loading styles
                this.comp.tpl = this.comp.renderer.findTemplateEl(this.cachedTplId, this.comp); // try to find overriden template
                if (!this.comp.tpl) {
                    this.comp.renderer.mountTemplate(
                        this.tplPath, this.cachedTplId,
                        this.comp, {
                            themePath: this.themePath
                        }).then((tpl) => {
                            this.comp.tpl = tpl;
                            this.doRender();
                    });
                } else {
                    this.doRender();
                }
            });
        }

    }

    getOrGenId() {
        let id;
        if (!this.comp.getAttribute('id')
            && this.idGenEnabled()) {
            id = this.comp.renderer.genElId(this.comp);
            this.comp.setAttribute('id', id);
        } else {
            id = this.comp.getAttribute('id');
        }
        return id;
    }

    get tplVars() {
        if (! this._tplVars) {
            this._tplVars = this.comp.tplVars ? this.comp.tplVars : {};
        }
        return this._tplVars;
    }

    set tplVars(tplVars) {
        this._tplVars = tplVars;
    }

    get i18n() {
        if (! this._i18n) {
            this._i18n = this.comp._i18n ? this.comp._i18n : {};
        }
        return this._i18n;
    }

    set i18n(i18n) {
        this._i18n = i18n;
    }

    renderWithVars(id, targetEl, tpl) {
        tpl = tpl || this.comp.tpl;
        if (! tpl) {
            return false;
        }
        targetEl = targetEl || this.comp.el;
        let el = this.comp.renderer.prepareTemplate(tpl);
        let rendered;
        if (this.comp.renderer.variableRender && this.comp.renderer.variableRender !== 'false') {
            rendered = this.comp.renderer.renderMustacheVars(el, {
                id: id,
                themePath: this.themePath,
                ...this.tplVars,
                ...this.i18n
            });
            targetEl.innerHTML = '';
            targetEl.innerHTML = rendered;
        } else {
            targetEl.innerHTML = '';
            targetEl.appendChild(el);
        }
    }

    fileNameFromUrl(url) {
        return url ? url.substring(url.lastIndexOf('/') + 1) : null;
    }

    indexMountedStyles() {
        if (this.comp.hasAttribute('styles') && this.comp.getAttribute('styles') === 'false') {
            return false;
        }
        let links = this.comp.el.querySelectorAll('link');
        this.mountedStyles = {};
        for (let link of links) {
            let fileName = this.fileNameFromUrl(link.getAttribute('href'));
            this.mountedStyles[fileName] = link;
        }
        let styles = this.comp.el.querySelectorAll('style');
        for (let style of styles) {
            let fileName = style.hasAttribute('data-file-name') ? style.getAttribute('data-file-name') : null;
            if (fileName) {
                this.mountedStyles[fileName] = style;
            }
        }
    }

    findParent(el, tag) {
        while (el.parentNode) {
            el = el.parentNode;
            if (el.tagName === tag)
                return el;
        }
        return null;
    }

    validateWithAttrs() {
        if (this.skFormParent) { // if has sk-form parents, rely on it
            return true;
        }
        this.hasOwnValidator = false;
        let validatorNames = this.skValidators ? Object.keys(this.skValidators) : [];
        for (let validatorName of validatorNames) { // :TODO check there is no parent sk-form first
            if (this.comp.hasAttribute(validatorName)) {
                this.hasOwnValidator = true;
                let validator = this.skValidators[validatorName];
                let result = validator.validate(this.comp);
                return result;
            }
        }
        if (! this.hasOwnValidator) { // any value is valid if no validator defined
            return true;
        }
        return false;
    }

    showInvalid() {
        if (this.comp.hasAttribute('validation-label') && this.comp.getAttribute('validation-label') !== 'disabled') {
            this.renderValidation();
        }
    }

    showValid() {
        if (this.comp.hasAttribute('validation-label') && this.comp.getAttribute('validation-label') !== 'disabled') {
            this.flushValidation();
        }
    }

    renderValidation(validity) {
        if (this.comp.hasAttribute('validation-label') && this.comp.getAttribute('validation-label') !== 'disabled') {
            let msgEl = this.comp.el.querySelector('.form-validation-message');
            if (! msgEl) {
                msgEl = this.comp.renderer.createEl('span');
                msgEl.classList.add('form-validation-message');
                msgEl.insertAdjacentHTML('afterbegin', validity || this.comp.validationMessage);
                this.comp.el.appendChild(msgEl);
            } else {
                msgEl.innerHTML = '';
                msgEl.insertAdjacentHTML('afterbegin', validity || this.comp.validationMessage);
            }
        }
    }

    flushValidation() {
        let msgEl = this.comp.el.querySelector('.form-validation-message');
        if (msgEl) {
            if (msgEl.parentElement) {
                msgEl.parentElement.removeChild(msgEl);
            } else {
                msgEl.parentNode.removeChild(msgEl);
            }
        }
    }

    getScrollTop() {
        if (typeof pageYOffset != 'undefined') {
            return pageYOffset;
        } else {
            let body = document.body;
            let document = document.documentElement;
            document = (document.clientHeight) ? document : body;
            return document.scrollTop;
        }
    }

    get actionSubElementsSl() {
        if (! this._actionSubElementsSl) {
            this._actionSubElementsSl = ACTION_SUBELEMENTS_SL;
        }
        return this._actionSubElementsSl;
    }

    set actionSubElementsSl(sl) {
        this._actionSubElementsSl = sl;
    }

    bindAction(button, action) {
        if (this['on' + action]) { // handler defined on custom element
            button.onclick = function(event) {
                this['on' + action]();
                this.comp.dispatchEvent(new CustomEvent(action, {
                    target: this.comp,
                    detail: { value: event.target.value },
                    bubbles: true,
                    composed: true
                }));
            }.bind(this);
        } else if (this.comp['on' + action]) { // handler defined in implementation
            button.onclick = function(event) {
                this.comp['on' + action]();
                this.comp.dispatchEvent(new CustomEvent(action, {
                    target: this.comp,
                    detail: {value: event.target.value},
                    bubbles: true,
                    composed: true
                }));
            }.bind(this);
        } else { // no handler binded just throw and event
            button.onclick = function(event) {
                this.comp.dispatchEvent(new CustomEvent(action, {
                    target: this.comp,
                    detail: { value: event.target.value },
                    bubbles: true,
                    composed: true
                }));
            }.bind(this);
        }
    }

    bindActions(target) {
        target = target ? target : this;
        let buttons = target.querySelectorAll(this.actionSubElementsSl);
        for (let button of buttons) {
            if (button.hasAttribute('action')) {
                let action = button.getAttribute('action');
                this.bindAction(button, action);
            }
            if (button.dataset.action) {
                let action = button.dataset.action;
                this.bindAction(button, action);
            }
        }
    }

    get subEls() {
        return [];
    }

    clearAllElCache() {
        for (let el of this.subEls) {
            if (this[el]) {
                this[el] = null;
            }
        }
        this.comp.clearElCache();
    }

    removeEl(el) {
        if (typeof el.remove === 'function') {
            el.remove();
        } else if (el.parentElement) {
            el.parentElement.removeChild(el);
        }
    }
}

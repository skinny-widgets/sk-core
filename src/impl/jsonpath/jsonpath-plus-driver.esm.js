

import { JSONPath } from '../../../../jsonpath-plus/dist/index-browser-esm.js';


export class JsonPathPlusEsmDriver {
    
    query(data, path) {
        let args = arguments;
        return JSONPath({path: path, json: data});
    }
}

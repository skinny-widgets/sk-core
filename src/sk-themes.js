
import { DefaultTheme } from "./default-theme.js";
import { ResLoader } from "../complets/res-loader.js";


export class SkThemes {

    static capitalizeFirstLetter(string) {
        return string.charAt(0).toUpperCase() + string.slice(1);
    }

    static byName(name, config) {
        if (name && name !== 'default') {
            let themePath = '/node_modules/sk-theme-' + name + '/src/' + name + '-theme.js';
            let className = SkThemes.capitalizeFirstLetter(name) + 'Theme';
            let dynImportOff = (config.hasAttribute('dimport') && config.getAttribute('dimport') === 'false');
            if (dynImportOff) {
                return ResLoader.loadClass(className, themePath, null, false, true);
            } else {
                return ResLoader.loadClass(className, themePath);
            }
        } else {
            return Promise.resolve(new DefaultTheme(config));
        }
    }
}
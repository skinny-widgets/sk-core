
import { Registry } from '../complets/registry.js';

export class CompRegistry extends Registry {

    /**
     * Overriden method to wire one definition, adds connections and plugins property support
     * @param definitionName
     * @param map
     * @returns {*}
     */
    initDef(definitionName, map) {
        this.registry[definitionName] = super.initDef(definitionName, map);
        if (map[definitionName] && map[definitionName].connections) {
            this.registry[definitionName].prototype.connections = map[definitionName].connections;
        }
        if (map[definitionName] && map[definitionName].plugins) {
            this.registry[definitionName].prototype.plugins = map[definitionName].plugins;
        }
        return this.registry[definitionName];
    }
}


export class SkNumberParser {
	
	constructor(locale, group, decimal, numeral) {
		this._locale = locale;
		let format = new Intl.NumberFormat(locale);
		let parts = { find: () => { return ''}};
		if (typeof format.formatToParts === 'function') {
			parts = format.formatToParts(12345.6);
		}
		let numerals = Array.from({ length: 10 }).map((_, i) => format.format(i));
		let index = new Map(numerals.map((d, i) => [d, i]));
		this._group = group || new RegExp(`[${parts.find(d => d.type === "group").value}]`, "g");
		this._decimal = decimal || new RegExp(`[${parts.find(d => d.type === "decimal").value}]`);
		this._numeral = numeral || new RegExp(`[${numerals.join("")}]`, "g");
		this._index = d => index.get(d);
	}
	
	parse(string, locale) {	
		return (string = string.trim()
			.replace(this._group, "")
			.replace(this._decimal, ".")
			.replace(this._numeral, this._index)) ? +string : NaN;
	}
}

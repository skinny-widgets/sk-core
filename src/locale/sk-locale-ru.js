


export var SkLocaleRu = {
    'Field value is invalid': 'Ошибка ввода данных',
    'This field is required': 'Это поле обязательное',
    'The value is less than required': 'Значение меньше допустимого',
    'The value is greater than required': 'Значение больше допустимого',
    'The value must me a valid email': 'Значение должно быть email адресом',
    'The value must match requirements': 'Значение должно соответствовать',
    'Ok': 'Ок',
    'Cancel': 'Отмена'
};
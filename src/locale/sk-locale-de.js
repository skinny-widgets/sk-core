


export var SkLocaleDe = {
    'Field value is invalid': 'Das ist ungültig',
    'This field is required': 'Dies ist erforderlich',
    'The value is less than required': 'Das muss größer sein',
    'The value is greater than required': 'Das muss weniger sein',
    'The value must me a valid email': 'Das muss eine E-Mail sein',
    'The value must match requirements': 'Dies muss den Anforderungen entsprechen',
    'Ok': 'Ок',
    'Cancel': 'Cancel'
};




export var SkLocaleCn = {
    'Field value is invalid': '不对',
    'This field is required': '必填',
    'The value is less than required': '太小',
    'The value is greater than required': '太大',
    'The value must me a valid email': '需要电子邮件',
    'The value must match requirements': '不对',
    'Ok': '好的',
    'Cancel': '消除'
};
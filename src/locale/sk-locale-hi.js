


export var SkLocaleHi = {
    'Field value is invalid': 'यह अमान्य है',
    'This field is required': 'यह आवश्यक है',
    'The value is less than required': 'यह अधिक होना चाहिए',
    'The value is greater than required': 'यह कम होना चाहिए',
    'The value must me a valid email': 'यह ईमेल होना चाहिए',
    'The value must match requirements': 'यह आवश्यकताओं से मेल खाना चाहिए',
    'Ok': 'Ок',
    'Cancel': 'रद्द करें'
};

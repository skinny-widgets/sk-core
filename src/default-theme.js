
import { SkTheme } from "./sk-theme.js";


export class DefaultTheme extends SkTheme {

    get basePath() {
        if (! this._basePath) {
            this._basePath = this.configEl ? `${this.configEl.basePath}/theme/default` : '/node_modules/skinny-widgets/theme/default';
        }
        return this._basePath;
    }

    get styles() {
        if (! this._styles) {
            this._styles = {
                'default.css': `${this.basePath}/default.css`
            };
        }
        return this._styles;
    }
}
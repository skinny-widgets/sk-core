
import { SK_RENDERED_EVT } from "./sk-element.js";

import { ConsoleLogger } from "../complets/log.js";

export function whenSkRendered(callback) {
    let whenDone = function() {
        callback();
    };
    if (window.skRendering && ! window.skRendered) {
        window.addEventListener(SK_RENDERED_EVT, function skReady() {
            window.removeEventListener(SK_RENDERED_EVT, skReady);
            ConsoleLogger.debug(`whenSkRendered run by ${SK_RENDERED_EVT} event`);
            whenDone();
        });
    } else {
        ConsoleLogger.debug('immediate whenSkRendered run');
        whenDone();
    }
}
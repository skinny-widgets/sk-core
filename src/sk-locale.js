
import { SkLocaleEn } from "./locale/sk-locale-en.js";
import { SkLocaleDe } from "./locale/sk-locale-de.js";
import { SkLocaleRu } from "./locale/sk-locale-ru.js";
import { SkLocaleCn } from "./locale/sk-locale-cn.js";
import { SkLocaleHi } from "./locale/sk-locale-hi.js";

export var LANGS_BY_CODES = {
    'en_US': 'EN',
    'en_UK': 'EN',
    'en': 'EN',
    'EN': 'EN',
    'us': 'EN',
    'ru_RU': 'RU',
    'ru': 'RU',
    'cn': 'CN',
    'CN': 'CN',
    'zh': 'CN',
    'de_DE': 'DE',
    'de': 'DE',
    'DE': 'DE',
	'hi-IN': 'HI',
	'hi_IN': 'HI',
	'HI': 'HI',
	'hi': 'HI'
};

export var LOCALES = {
    'EN': SkLocaleEn,
    'DE': SkLocaleDe,
    'RU': SkLocaleRu,
    'CN': SkLocaleCn,
    'HI': SkLocaleHi
};

export class SkLocale {

    constructor(lang) {
        this.lang = LANGS_BY_CODES[lang];
    }

    tr(string) {
        if (this.lang) {
            return LOCALES[this.lang][string] ? LOCALES[this.lang][string] : string;
        } else {
            return SkLocaleEn[string] ? SkLocaleEn[string] : string;
        }
    }
}


import { SkElement } from "./sk-element.js";
import { CompRegistry } from "./comp-registry.js";

export const REG_ITEM_TAG_NAME = 'sk-registry-item';
export const CONN_ITEM_TAG_NAME = 'sk-registry-connection';
export const CONN_ITEM_ATTRS = [
    'from', 'source-type', 'source', 'to', 'target-type', 'target', 'value'
];

export class SkRegistry extends SkElement {

    get itemTn() {
        return REG_ITEM_TAG_NAME;
    }

    get connectionTn() {
        return CONN_ITEM_TAG_NAME;
    }

    buildDeps() {
        this.deps = new Set();
        let items = this.querySelectorAll(this.itemTn);
        for (let item of items) {
            let dep = this.attrsToJson([
                'key', 'is', 'defName',
                { name: 'val', type: 'object'},
                { name: 'plugins', type: 'array' },
                { name: 'deps', type: 'object'}
            ], item, true);
            if (item.hasAttribute('plugins')) {
                dep.plugins = JSON.parse(item.getAttribute('plugins'));
            }
            if (item.hasAttribute('full-path')) {
                dep.path = item.getAttribute('full-path');
            } else {
                if (item.hasAttribute('path') && this.basePath) {
                    if (this.basePath) {
                        dep.path = this.basePath + '/' + item.getAttribute('path');
                    } else {
                        dep.path = item.getAttribute('path');
                    }
                }
            }
            if (item.hasAttribute('force-load')) {
                dep.forceLoad = item.getAttribute('force-load');
            }
            //dep.def = (Function('return ' + item.getAttribute("def")))();
            let depConnections = [];
            let connections = item.querySelectorAll(this.connectionTn);
            for (let connection of connections) {
                let depConnection = this.attrsToJson(CONN_ITEM_ATTRS, connection, true);
                depConnections.push(depConnection);
            }
            if (depConnections.length > 0) {
                dep.connections = depConnections;
            }
            this.deps[dep.key] = dep;
        }
    }

    setup() {
        //this.setupRenderer();
        if (this.configEl && this.configEl.configureElement) {
            this.configEl.configureElement(this);
            if (this.reflective && this.reflective !== 'false') {
                this.configEl.addEventListener('configChanged', this.onReflection.bind(this));
            }
        } else {
            if (this.configEl && this.configEl.hasAttribute('base-path')) {
                this.setAttribute('base-path', this.configEl.getAttribute('base-path'));
            }
            if (! this.hasAttribute('base-path')) {
                this.logger.warn(this.constructor.name, " ",
                    "configEl not found or sk-config element not registred, " +
                    "you will have to add base-path and theme(if not default) attributes for each sk element");
            }
            if (this.configEl && this.configEl.hasAttribute('dimport')) {
                this.setAttribute('dimport', this.configEl.getAttribute('dimport'));
            }
        }
/*        if (this.useShadowRoot) {
            if (! this.shadowRoot) {
                this.attachShadow({mode: 'open'});
            }
        }*/
    }

    loadDepsAndCons() {
        this.buildDeps();
        this.registry = window.registry || new CompRegistry();
        let dynImportOff = (this.hasAttribute('dimport') && this.getAttribute('dimport') === 'false');
        this.registry.wire(this.deps, dynImportOff);
        this.setupConnections();
    }

    get connections() {
        if (! this._connections) {
            let connections = Array.from(this.querySelectorAll(this.connectionTn))
                .filter(ele => ele.parentElement === this);
            let connectionsJson = [];
            for (let connection of connections) {
                connectionsJson.push(this.attrsToJson(CONN_ITEM_ATTRS, connection, true));
            }
            this._connections = connectionsJson;
        }
        return this._connections;
    }

    connectedCallback() {
        this.setup();
        if (this.hasAttribute('after-content')) {
            window.addEventListener('DOMContentLoaded', this.loadDepsAndCons.bind(this));
        } else {
            this.loadDepsAndCons();
        }

    }
}

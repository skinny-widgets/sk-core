

export const SK_RENDER_EVT = 'skrender';

export class SkRenderEvent extends CustomEvent {
    constructor(options) {
        super(SK_RENDER_EVT, options);
    }
}

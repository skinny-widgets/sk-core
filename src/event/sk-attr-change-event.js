

export const SK_ATTR_CHANGE_EVT = 'skattrchange';

export class SkAttrChangeEvent extends CustomEvent {
    constructor(options) {
        super(SK_ATTR_CHANGE_EVT, options);
    }
}
